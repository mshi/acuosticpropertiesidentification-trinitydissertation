# Identification of systematic difference in acoustic properties  in categories of laughter in  "MULTISIMO" dataset


[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

This repository contains all the code utilized for the dissertation titled 'Identification of systematic difference in acoustic properties  in categories of laughter in  "MULTISIMO" dataset'. The code corresponds to the methodologies,results and experiments described therein.

## Dataset

The dataset used in this research can be accessed through the following link:

[Dataset URL](https://multisimo.eu/datasets.html)

## Paper link 
[Paper URL](https://www.researchgate.net/publication/381965183_Identification_of_systematic_differences_in_acoustic_properties_in_categories_of_laughter_in_MULTISIMO_dataset)

## Reference Study

The following study was replicated as part of the dissertation:

- Tanaka, H., & Campbell, N. (2014). Classification of social laughter in natural conversational speech. *Computer Speech & Language*, 28(1), 314-325.

## Licensing Information

This project is distributed under the MIT License. For more details, see the [LICENSE.md](LICENSE) file included in this repository.

## Abstract
Research into laughter classification is a compelling field that captivates scientists and sociologists seeking to unravel the enigmatic nature of this social signal. This paralinguistic cue possesses a notably intricate acoustic structure. Unveiling its discriminating properties could shed light on the internal acoustic structure of laughter. Previous studies have undertaken experiments to identify these discriminating acoustic properties, presenting a comprehensive pipeline that spans machine learning selection, identification of discriminating properties, and exploring factors influencing them. However, previous research has not released its dataset publicly, and some procedures require enhancement.  To construct a more rigorous pipeline and comprehensively analyse discriminating acoustic properties, we compiled our dataset tailored to our research objectives from the "MULTISIMO" raw corpus(Multimodal and Multiparty Social Interactions Modelling), followed by identifying discriminating properties in mirthful and discourse laughter within our constructed dataset,
performing regression analysis on the datasets to identify significant features that could explain discourse and mirthful laughter ,and exploring factors influencing discriminative acoustic properties. 


The main findings in our work highlight that through a synthesis of the results from the machine learning experiments and regression analysis, we identified five shared discriminating acoustic properties across both experiments and laughter types: fundamental frequency, mel-frequency cepstral coefficient, auditory spectrum, spectral features, and jitter. The first four properties gauge energy-related information in acoustic laughter, while the last describes temporal characteristics. Our findings exhibit both concurrence and disparity with the findings from Tanaka and Campbell(2014),our replicated work, attributable to differences in the acoustic feature set quantity and the total number of utterance instances. Notably, fundamental frequency and spectral features emerge as common discriminating properties in both studies.



This work makes significant contributions both in theory and practice. Theoretically, this research has established a comprehensive pipeline encompassing dataset construction, verification, machine learning design and implementation, identification of acoustic properties, and examination of factors that may influence discriminating properties. This pipeline presents a novel avenue for researchers in audio processing and artificial intelligence. In terms of practical applications, although the study emphasises lies in theory, the developed algorithm shows promise for integration into real-time video systems to assist in laughter classification. It enables dynamic tracking of specific acoustic properties unique to instances of laughter.


## Authors

- **Mingwei Shi** - [mshi@tcd.ie](mailto:mshi@tcd.ie)

The document details the repository structure for the research project.

# Python Environment Specification

## Python Version

- Python 3.6

## Required Packages and Versions

| Package        | Version |
|----------------|---------|
| intervaltree   | 3.1.0   |
| ipython        | 8.15.0  |
| lxml           | 4.9.3   |
| numpy          | 1.24.3  |
| opensmile      | 2.4.2   |
| pandas         | 1.4.1   |
| pydub          | 0.25.1  |
| pympi_ling     | 1.70.2  |
| tqdm           | 4.65.0  |

## Directory Structure

The project is structured into four primary directories, each serving a specific function as outlined below:

### 1. Initial Dataset Construction

This directory contains scripts for constructing annotation files from the original EAF file, labeled as "[laugh]-Discourse".

- **MainProgram.py**: Main script for project execution.

### 2. Varied Duration Dataset

This directory includes scripts for creating a dataset with varied durations using 18 session CSVs from the initial dataset construction and corresponding audio files.

- **MainProgram.py**: Main script for project execution.

### 3. Fixed Duration Dataset

This directory houses the scripts for creating a fixed duration dataset (200 ms) using 18 session CSVs from the initial dataset construction and corresponding audio files.

- **MainProgram.py**: Main script for project execution.

### 4. Machine Learning Experiment

This folder contains Jupyter notebooks for conducting substantial machine learning experiments.



