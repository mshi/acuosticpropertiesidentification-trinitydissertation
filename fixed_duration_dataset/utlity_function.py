class utlity_function:
    def __init__(self):
        pass

    """
          Processes a data frame by locating the row where the column 'end time-ms'
          equals the given value and then deleting all rows after this row. If this row is the last, do nothing.
    """

    def calibrate_dataframe(self,df, given_value):
        # Find the index of the row where 'end time-ms' equals given value
        index = df[df['end time-ms'] == given_value].index

        # If the row is found and it is not the last row, delete all rows after it
        if not index.empty and index[0] < len(df) - 1:
            df = df.iloc[:index[0] + 1]

        return df

    def find_top_10_max_values(self,my_list):
        return sorted(my_list, reverse=True)[:10]

    # Format the number to show two digits, adding a leading zero if necessary.
    def format_number(self,number):
        return f"{number:02d}"

    # Function to flatten a list of lists into a single list
    def flatten_list_of_lists(self,list_of_lists):
        return [item for sublist in list_of_lists for item in sublist]

    """Function to find the value in the list whose absolute difference with a given value is minimum."""

    def find_closest_value(self,my_list, given_value):
        return min(my_list, key=lambda x: abs(x - given_value))
