from statistics import median

# We need to check thr

def twoCSVmerge_algorithm(list1,list2):
    #pointer
    pointer_list2 = 0
    #dyanamics
    thrsold = 50
    for row in list1:
        row__in_list2 = list2[pointer_list2]

        start_time = row[0]
        ent_time = row[1]


        start_time_list_2 = row__in_list2[0]
        end_time_list2 = row__in_list2[1]
        #it means this short moment is overlaped by the temproal segmentation
        # it skip
        if (start_time!=start_time_list_2 or end_time_list2!=end_time_list2):
            pointer_list2+=1
            row__in_list2 = list2[pointer_list2]
            start_time_list_2 = row__in_list2[0]
            end_time_list2 = row__in_list2[1]



        if(start_time>=start_time_list_2 and end_time_list2<=end_time_list2):
            # add information here
            pass
        elif (start_time>=start_time_list_2 and ent_time >end_time_list2):
            #compute threshold here
            distance_disprancty = abs(ent_time-end_time_list2)
            if distance_disprancty <=thrsold:
                #same catgeory
            else:
                #belong to next category


def threshold_selection_algorithm(list1: list[str], list2: list[str]) -> int:
    # Calculate distance
    distances = []

    for range1 in list1:
        in_range = False
        for range2 in list2:
            if range1[0] >= range2[0] and range1[1] <= range2[1]:
                in_range = True
                break

        if not in_range:
            nearest_end_distance = min(abs(range1[1] - range2[1]) for range2 in list2)
            distances.append(nearest_end_distance)

    unique_numbers = list(set(distances))
    sort_distance = sorted(unique_numbers)

    median_in_list: int = int(median(sort_distance))

    threshold: int = median_in_list

    return threshold
