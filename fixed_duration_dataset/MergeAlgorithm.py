import csv
import glob
import os
import pandas as pd
from tqdm import tqdm

# This is newest threshodl algorithm
class MergeAlgo:
    def __init__(self,inference_csv_path,opensimile_csv_path,sessionName):
        self.inference_csv_path = inference_csv_path
        self.opensimile_csv_path = opensimile_csv_path
        self.sessionName = sessionName
        self.global_final_list = []

        # Get participant list here

    # Changing rows with 'null' in 'concise merge type' column to 'Ambiguous'

    def preprocessingData(self,inputDF):

        # Changing rows with 'null' in 'concise merge type' column to 'Ambiguous'

        processedDF = inputDF.loc[inputDF['concise merge type'].str.contains('null'), 'concise merge type'] = 'Ambiguous'

        return processedDF

    # when calling Opensimile it exist the list of list
    def flatten_list_of_lists(self,lst_of_lsts):
        return [item for sublist in lst_of_lsts for item in sublist]
    def find_interval_Opensimile(self,ELAN_list,OpensimileListOfList):

        startTimeElan = ELAN_list[0]
        endTimeElan = ELAN_list[1]

        for inner_list in OpensimileListOfList:
            startTimeOpensimile = inner_list[0]
            endTimeOpensimile = inner_list[1]

            firstcondition = (startTimeElan >= startTimeOpensimile)
            secondCondtion = (endTimeElan <= endTimeOpensimile)

            condition = firstcondition and secondCondtion

            if condition == True:
                return_list = [startTimeOpensimile,endTimeOpensimile]
                return return_list



        return []





    def find_nearest_start_point_inOpensimile(self,value, opensimileInterval):
        """
        Given the middle point of the opensimile and list of list OpensimileInterval
        """
        # Extract the first element from each list
        first_elements = [lst[0] for lst in opensimileInterval if lst]

        # Find the nearest value
        nearest_value = min(first_elements, key=lambda x: abs(x - value))

        return nearest_value

    def find_nearest_end_point_inOpensimile(self,value, opensimileInterval):
        """
        Given the middle point of the opensimile and list of list OpensimileInterval
        """
        # Extract the second element from each list
        first_elements = [lst[1] for lst in opensimileInterval if lst]

        # Find the nearest value
        nearest_value = min(first_elements, key=lambda x: abs(x - value))

        return nearest_value

    def judgeIntervalWithinRelationship(self,ELAN_LIST, Opensimile_List):
        start_time_ELAN = ELAN_LIST[0]
        end_time_ELAN = ELAN_LIST[1]

        for inner_list in Opensimile_List:
            start_time_inner_list_opensimile = inner_list[0]
            end_time_inner_list_opensimile = inner_list[1]

            firstcondition = (start_time_ELAN >= start_time_inner_list_opensimile)
            secondCondtion = (end_time_ELAN <= end_time_inner_list_opensimile)

            condition = firstcondition and secondCondtion

            if condition == True:
                return True
            else:
                return False
    def get_featureList_index(self) -> list[str]:
        file_paths = 'feature_name_list_compare.txt'
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]

        feature_name_list = content_list

        return feature_name_list

    def divisor_judge(self,dividend, divisor):
        if divisor == 0:
            return False
        return dividend % divisor == 0
    def export_csv(self):
        featurelist = self.get_featureList_index()
        # Seesion id, participant id
        # Start time ,end time
        # Duration
        # CV-merge-M-L-S  'concise merge type
        # Count for this type First appear
        # 6,373 Acuostic features
        basicInformation = ['Session id', 'Participant id', 'Start Time - ms',
                            'End Time - ms', 'Duration - ms',
                            'CV - merge - M - L - S', 'concise merge type',
                            'Count for the same utterance type',
                            'First time appear',

                            ]
        final_index = basicInformation + featurelist
        self.global_final_list.insert(0, final_index)
        post_fix = '-merge-constant.csv'
        fileName = self.sessionName+post_fix


        final_intermidate_save_name:str = fileName
        with open(final_intermidate_save_name, mode='w', newline='') as file:
            writer = csv.writer(file)

            # Wrap your loop with tqdm for the progress bar
            for row in tqdm(self.global_final_list, desc="Writing rows"):
                writer.writerow(row)


    def execute(self):
        current_ELAN_df = pd.read_csv(self.inference_csv_path)
        # Ambigouous
       # current_ELAN_df = self.preprocessingData(current_ELAN_df)
        #
        current_ELAN_df.loc[current_ELAN_df['concise merge type'].str.contains('null'), 'concise merge type'] = 'Ambiguous'

        current_Opensimile_df = pd.read_csv(self.opensimile_csv_path)
        # We need to confirm the player list here from ELAN dataset
        # Bug here
        player_list = current_ELAN_df['Participant id'].tolist()

        unique_set = set(player_list)
        unique_player_list = list(unique_set)
        fixed_duration_opensimile = 200
        anchor_saturation = 120 / 200

        # self.global_final_list = []

        # self.global_final_list = []

        # Iterate the player list
        for player in unique_player_list:
            # first extract subset dataframe of both df

            current_player_ELAN_DF = current_ELAN_df[current_ELAN_df['Participant id'] == player]
            current_player_ELAN_DF_resetIndex = current_player_ELAN_DF.reset_index(drop=True)

            current_player_opensimile_DF = current_Opensimile_df[current_Opensimile_df['participant id'] == player]
            current_player_opensimile_DF_resetIndex = current_player_opensimile_DF.reset_index(drop=True)

            # Then we need to set the pointer to iterate the start position of opensimile
            pointer_opensimile:int = 0
            # Iterate each row of current_player_ELAN_DF
            button_for_move_next_event:bool = False
            print("Inital setting finished !")
            print('-----------------------\n')
            print("Currenet player is "+player)
            print('-----------------------\n')
            for index, row in current_player_ELAN_DF_resetIndex.iterrows():

                duration_elan: int = row['Duration-ms']

                # First we need to determine the length of ELAN is greater than or
                # equalt to 200
                integer_part:int = int(duration_elan/fixed_duration_opensimile)

                # for reminder and the duration is less than 200
                reminder :int= duration_elan%fixed_duration_opensimile

                number_intervals:int = integer_part

                #Count and first time appear are the mutually veirficed
                first_appear = False
                count_this_type = 0

                count_this_type = 0
                # it meast the reminder < 120/200
                if button_for_move_next_event == True:
                    count_this_type += 1
                    number_intervals -= 1
                # When finished it should be shutdown immedicately
                button_for_move_next_event = False

                if duration_elan<200:
                    # less than 200:
                    # need to change type = 0
                    #count_this_type = 0

                    # current row
                    # Basic information
                    count_this_type += 1
                    session_id = row['Session id']
                    pariticpant_id = row['Participant id']
                    mergeCLS = row['CV-merge-M-L-S']
                    concise_type = row['concise merge type']
                    # start time
                    start_time_openSimile = pointer_opensimile

                    end_time_openSimile = pointer_opensimile + fixed_duration_opensimile

                    pointer_opensimile += fixed_duration_opensimile

                    # Query for Opensimile dataset and extract dataset
                    # current_player_opensimile_DF_resetIndex
                    # Query the DataFrame
                    queried_row_openSimile = current_player_opensimile_DF_resetIndex[
                        (current_player_opensimile_DF_resetIndex['start time-ms'] == start_time_openSimile) & (
                                current_player_opensimile_DF_resetIndex['end time-ms'] == end_time_openSimile)]

                    featurelist = self.get_featureList_index()
                    acuosticFeature = queried_row_openSimile[featurelist]
                    acuosticFeatureList = acuosticFeature.values.tolist()
                    acuosticFeatureList = self.flatten_list_of_lists(acuosticFeatureList)

                    if count_this_type == 1:
                        first_appear = True
                    else:
                        first_appear = False

                    basic_Portoloi = [
                        session_id, pariticpant_id,
                        start_time_openSimile, end_time_openSimile,
                        fixed_duration_opensimile,
                        mergeCLS, concise_type,
                        count_this_type, first_appear
                    ]

                    merge_list_row = basic_Portoloi + acuosticFeatureList

                    print(merge_list_row)
                    segmentLine = "--------------------------------------"
                    print(segmentLine)
                    print("End time is " + str(end_time_openSimile))

                    self.global_final_list.append(merge_list_row)

                else:
                    print("Enter Integer part")
                    print("---------------\n")
                    for i in range(number_intervals):
                        count_this_type += 1

                        start_time_openSimile = pointer_opensimile

                        end_time_openSimile = pointer_opensimile + fixed_duration_opensimile

                        pointer_opensimile += fixed_duration_opensimile

                        # Query for Opensimile dataset and extract dataset
                        # current_player_opensimile_DF_resetIndex
                        # Query the DataFrame
                        queried_row_openSimile = current_player_opensimile_DF_resetIndex[
                            (current_player_opensimile_DF_resetIndex['start time-ms'] == start_time_openSimile) & (
                                    current_player_opensimile_DF_resetIndex['end time-ms'] == end_time_openSimile)]

                        featurelist = self.get_featureList_index()
                        acuosticFeature = queried_row_openSimile[featurelist]
                        acuosticFeatureList = acuosticFeature.values.tolist()
                        acuosticFeatureList = self.flatten_list_of_lists(acuosticFeatureList)

                        # Query session if from ELAn
                        # current_player_ELAN_DF_resetIndex
                        # Row current row
                        session_id = row['Session id']
                        pariticpant_id = row['Participant id']
                        mergeCLS = row['CV-merge-M-L-S']
                        concise_type = row['concise merge type']

                        if count_this_type == 1:
                            first_appear = True
                        else:
                            first_appear = False

                        # Seesion id, participant id
                        # Start time ,end time
                        # Duration
                        # CV-merge-M-L-S  'concise merge type
                        # Count for this type First appear
                        # 6,373 Acuostic features
                        basic_Portoloi = [
                            session_id, pariticpant_id,
                            start_time_openSimile, end_time_openSimile,
                            fixed_duration_opensimile,
                            mergeCLS, concise_type,
                            count_this_type, first_appear
                        ]

                        merge_list_row = basic_Portoloi + acuosticFeatureList

                        print(merge_list_row)
                        segmentLine = "--------------------------------------"
                        print(segmentLine)

                        print("End time is " + str(end_time_openSimile))

                        self.global_final_list.append(merge_list_row)
                    print("Enter reminder part")
                    print("---------------\n")
                    # then remnder part
                    if reminder != 0:
                        interval_usage = reminder / fixed_duration_opensimile
                        # Then process with reminder
                        interval_usage = reminder / fixed_duration_opensimile
                        # if intervalUsage >= anchor saturation

                        if interval_usage >= anchor_saturation:
                            # current row
                            # Basic information

                            count_this_type += 1
                            session_id = row['Session id']
                            pariticpant_id = row['Participant id']
                            mergeCLS = row['CV-merge-M-L-S']
                            concise_type = row['concise merge type']
                            # start time
                            start_time_openSimile = pointer_opensimile

                            end_time_openSimile = pointer_opensimile + fixed_duration_opensimile

                            pointer_opensimile += fixed_duration_opensimile

                            # Query for Opensimile dataset and extract dataset
                            # current_player_opensimile_DF_resetIndex
                            # Query the DataFrame
                            queried_row_openSimile = current_player_opensimile_DF_resetIndex[
                                (current_player_opensimile_DF_resetIndex[
                                     'start time-ms'] == start_time_openSimile) & (
                                        current_player_opensimile_DF_resetIndex[
                                            'end time-ms'] == end_time_openSimile)]

                            featurelist = self.get_featureList_index()
                            acuosticFeature = queried_row_openSimile[featurelist]
                            acuosticFeatureList = acuosticFeature.values.tolist()
                            acuosticFeatureList = self.flatten_list_of_lists(acuosticFeatureList)

                            if count_this_type == 1:
                                first_appear = True
                            else:
                                first_appear = False

                            basic_Portoloi = [
                                session_id, pariticpant_id,
                                start_time_openSimile, end_time_openSimile,
                                fixed_duration_opensimile,
                                mergeCLS, concise_type,
                                count_this_type, first_appear
                            ]

                            merge_list_row = basic_Portoloi + acuosticFeatureList

                            print(merge_list_row)
                            segmentLine = "--------------------------------------"
                            print(segmentLine)
                            self.global_final_list.append(merge_list_row)
                            print("End time is " + str(end_time_openSimile))


                        # else  intervalUsage < anchor saturation
                        else:
                            # pointer to the next index
                            nextIndex = index + 1
                            # Bug here
                            # Next type
                            lengthOfCurrwtnDF = len(current_player_ELAN_DF_resetIndex)

                            if nextIndex != lengthOfCurrwtnDF:

                                indexRow = current_player_ELAN_DF_resetIndex.iloc[nextIndex]

                                # query for basic information
                                session_id = indexRow['Session id']
                                pariticpant_id = indexRow['Participant id']
                                mergeCLS = indexRow['CV-merge-M-L-S']
                                concise_type = indexRow['concise merge type']

                                count_this_type = 1
                                if count_this_type == 1:
                                    first_appear = True
                                else:
                                    first_appear = False

                                button_for_move_next_event = True

                                # Time information
                                # start time
                                start_time_openSimile = pointer_opensimile

                                end_time_openSimile = pointer_opensimile + fixed_duration_opensimile

                                pointer_opensimile += fixed_duration_opensimile

                                # acuostic feature
                                # Query for Opensimile dataset and extract dataset
                                # current_player_opensimile_DF_resetIndex
                                # Query the DataFrame
                                queried_row_openSimile = current_player_opensimile_DF_resetIndex[
                                    (current_player_opensimile_DF_resetIndex[
                                         'start time-ms'] == start_time_openSimile) & (
                                            current_player_opensimile_DF_resetIndex[
                                                'end time-ms'] == end_time_openSimile)]

                                featurelist = self.get_featureList_index()
                                acuosticFeature = queried_row_openSimile[featurelist]
                                acuosticFeatureList = acuosticFeature.values.tolist()
                                acuosticFeatureList = self.flatten_list_of_lists(acuosticFeatureList)

                                basic_Portoloi = [
                                    session_id, pariticpant_id,
                                    start_time_openSimile, end_time_openSimile,
                                    fixed_duration_opensimile,
                                    mergeCLS, concise_type,
                                    count_this_type, first_appear
                                ]

                                merge_list_row = basic_Portoloi + acuosticFeatureList

                                print(merge_list_row)
                                segmentLine = "--------------------------------------"
                                print(segmentLine)
                                print("End time is " + str(end_time_openSimile))
                                self.global_final_list.append(merge_list_row)





'''


if __name__ == '__main__':
    ELAN_Sample_csv_path = 'S02-inference_subset.csv'
    Opensimile_sample_csv_path = 'sampleOpensimileSession2-first50row-each-player.csv'

    myTestInstance = MergeAlgo(ELAN_Sample_csv_path,Opensimile_sample_csv_path)

    myTestInstance.execute()

    myTestInstance.export_csv()

'''
