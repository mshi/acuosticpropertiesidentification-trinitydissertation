import glob
import os
import pandas as pd
import opensmile

import audiosegment

import time

from OneSessionFile_PATH import OneSessionFile_PATH
from paricipantCodeChunk import paricipantCodeChunk

# This class cut the duration
class OneSession_OpensimileDataFrame:
    def __init__(self,duration,sessionNumber):

        self.duration = duration

        self.sessionNumber = sessionNumber

        path_getter = OneSessionFile_PATH(sessionNumber=self.sessionNumber)
        self.myCSVpath = path_getter.get_csv_path()
        self.my_audio_path_collection = path_getter.get_audio_path_list()


        self.sessionID = ''
        self.final_mergeList= []




    def get_session_id(self,audio_path):
        audofilepath: str =audio_path
        # The filter one
        # for paricipant code extraction
        # first extract the root file name
        root_dire_file_name = audofilepath.rsplit('/', 1)[-1]
        file_name_without_extension: str = root_dire_file_name.replace(".wav", "")
        split_file_str = file_name_without_extension.split('_')
        session_id = split_file_str[1]

        return session_id

    def get_participaint_id(self,audio_path):
        audofilepath: str = audio_path
        # The filter one
        # for paricipant code extraction
        # first extract the root file name
        root_dire_file_name = audofilepath.rsplit('/', 1)[-1]
        myChunk = paricipantCodeChunk(root_dire_file_name)
        returnparticiant_code = myChunk.get_final_particpant_code()

        return returnparticiant_code

# Get tuple list [
    #[0,100],[100,200].....]
    def acquire_audio_segment_sequence(self,audio_path):
        myfile_path =audio_path
        audio = audiosegment.from_file(myfile_path)

        # set 100 mileseconds
        # as the total audio length is 622822 is great than the eaf end time 622817

        # This fine grained level as 622822
        segment_duration = self.duration
        audolength = len(audio)
        # List to store audio segments
        segmentsTuplist = []

        # Iterate through the audio file and segment it
        for i in range(0, audolength, segment_duration):
            start_time = i
            end_time = i + segment_duration
            print(start_time)
            print(end_time)
            tuple_temp = (start_time, end_time)
            print("_--------------")
            segmentsTuplist.append(tuple_temp)

        self.segmentTuple = segmentsTuplist
        return segmentsTuplist
    def segment_identical_moment_audio(self):


        current_audio_list = self.my_audio_path_collection
        # Define marco
        FIXED_DUARATION:int = self.duration
        #load into the audioSegment file
       # audofilepath:str = self.audio_path
       #
####################################
        for current_audio_path in current_audio_list:
            current_segment_tuple:list[(int,int)] =self.acquire_audio_segment_sequence(current_audio_path)
            current_session_id = self.get_session_id(audio_path=current_audio_path)
            self.sessionID = current_session_id
            current_participant_id = self.get_participaint_id(audio_path=current_audio_path)
            input_audio_file = audiosegment.from_file(current_audio_path)
            for eachTuple in current_segment_tuple:

                startTime: int = eachTuple[0]
                endTime: int = eachTuple[1]
                # start segment
                current_audio_segment = input_audio_file[startTime:endTime]

                # outfilename = folder + '-' + 'discourse' + '-' + str(index) + '.wav'

                hypen = '-'
                file_post_fix = '.wav'
                # outfilename_aidio = current_seesion_id + '-' + p_d + '-' + conversation_type_s +'-'+startTime+'+'.wav'
                outfilename_audio = str(startTime) + hypen + str(
                    endTime) + file_post_fix

                current_audio_segment.export(outfilename_audio, format="wav")
                print(f"Audio segment saved to {outfilename_audio}")
                ####################################
                # Loading already
                print("Staring using Oensimile")

                smile = opensmile.Smile(
                    feature_set=opensmile.FeatureSet.ComParE_2016,
                    feature_level=opensmile.FeatureLevel.Functionals,
                )
                start_time_counter = time.perf_counter()

                result_df_current = smile.process_file(outfilename_audio)

                end_time_counter = time.perf_counter()
                runtime = end_time_counter - start_time_counter
                print(f"The line of code took {runtime} seconds to run.")

                dfcurrent_T = result_df_current.T

                dfcurrent_T.reset_index(inplace=True)

                list_of_listssss = dfcurrent_T.values.tolist()

                feature_value_list = []

                # Iterate through the list of lists
                for inner_list in list_of_listssss:
                    # feature_name_list.append(inner_list[0])  # Add the first element to the first list

                    feature_value_list.append(inner_list[1])  # Add the second element to the second list
                #
                # start time | end time | duration | Acuostic feature
                inforation_list = [current_session_id, current_participant_id, startTime, endTime, FIXED_DUARATION]
                curr_merge_list = inforation_list + feature_value_list
                self.final_mergeList.append(curr_merge_list)
                # Delte file name

                wav_files = glob.glob('*.wav')
                # Remove current
                for wav_file in wav_files:
                    try:
                        os.remove(wav_file)
                        print(f"Deleted {wav_file}")
                    except OSError as e:
                        print(f"Error: {wav_file} : {e.strerror}")

                print("-------------")


    def export_pandas_df(self):
        file_paths = 'feature_name_list_compare.txt'
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]

        feature_name_list = content_list
        # add origianl one
        basic_info = [
            'session id', 'participant id', 'start time-ms', 'end time-ms', 'duration-ms'
        ]

        finalIndex = basic_info + feature_name_list
        # list of list
        final_List = self.final_mergeList

        current_df = pd.DataFrame(final_List, columns=finalIndex)

        # Deleting rows with any NA values
        #current_df.dropna(inplace=True)

        outputfileName= self.sessionID+'-'+'segmentedby'+str(self.duration)+"ms"+'.csv'
        current_df.to_csv(outputfileName)

        return current_df


    def export_final_csv(self):

        file_paths = 'feature_name_list_compare.txt'
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]

        feature_name_list = content_list
        # add origianl one
        basic_info = [
            'session id','participant id','start time-ms', 'end time-ms', 'duration-ms'
        ]

        finalIndex = basic_info+feature_name_list
        # list of list
        final_List = self.final_mergeList

        current_df = pd.DataFrame(final_List, columns=finalIndex)

        output_string_name = self.session_id+'-'+'opensimile'+'.csv'

        current_df.to_csv(output_string_name)

        return current_df


if __name__ == '__main__':

    #input_audio_file = audiosegment.from_file(file_path)
    #current_audio_segment = input_audio_file[0:100]

    print("sss")
    session_id_list = [2,3,4, 5, 7, 8, 9, 10, 11, 13, 14, 17, 18, 19, 20, 21, 22, 23]

    duration_list = [200]
    for current_duration in duration_list:
        specificSession_number = current_duration
        for currenrt_id in session_id_list:
            specificSession_number: int = currenrt_id
            myOneSession_segment = OneSession_OpensimileDataFrame(current_duration,specificSession_number)
            myOneSession_segment.segment_identical_moment_audio()
            myOneSession_segment.export_pandas_df()