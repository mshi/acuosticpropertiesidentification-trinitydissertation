import os
from itertools import chain
import pandas as pd
from statistics import median

from MergeAlgorithm import MergeAlgo
from OneSession_OpensimileDataFrame import OneSession_OpensimileDataFrame
from utlity_function import utlity_function


class MergeTwoCSV_Constant:
    def __init__(self, session_number):
        self.first_time = False
        self.counter_for_this_type = 0
        self.session_number = session_number
        # Utility function here
        self.myUtility_function = utlity_function()
        self.session_id: str = 'S' + self.myUtility_function.format_number(self.session_number)
        self.pointer_list_current = 0
        # Get the path for openSimile
        self.openSimile_csv_path = ''
        self.current_thrshold = 0
        # Get OpenSimile Address
        self.opensimile_address_function()
        #  # Get the path for Inferencedataset ELAN csv
        self.inference_path = ''
        self.inference_csv_addres_function()

        # Loading paricipant code here
        self.particpiant_list: list[str] = []
        self.acquire_paricpant_list()

        # Max time for preprocessing
        self.max_time_inference_dataset: int = 0
        self.acquire_max_time_inference_dataset()

        # Theshold csv
        self.threshold_list = []
        # final merge list
        self.final_merge_list = []

        # Main code

    def get_feature_list(self):
        file_paths = 'feature_name_list_compare.txt'
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]

        feature_name_list = content_list
        return feature_name_list

    def get_first_time_button_str(self, first_time_button):
        return 'Y' if first_time_button else 'N'

    def export_threshold_information(self):

        index_column = ['Session number', 'Speaker', 'Threshold']
        current_df = pd.DataFrame(self.threshold_list, columns=index_column)
        middle_stage_name: str = self.myUtility_function.format_number(self.session_number) + '-interim-Threshold.csv'
        current_df.to_csv(middle_stage_name)

        print("Genaeate the final one")
        final_name = 'S' + self.myUtility_function.format_number(self.session_number) + '-Threshold.csv'
        final_df = pd.read_csv(middle_stage_name)
        final_df.drop(columns=final_df.columns[0], axis=1, inplace=True)
        final_df.to_csv(final_name, index=False)

    def export_final_merge_csv(self):
        file_paths = 'feature_name_list_compare.txt'
        self.counter_for_this_type =0
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]
        # 6373
        feature_name_list = content_list
        # 9
        # Format
        # Session id | Particpaint id| start time-ms | end time-ms
        # duration -ms  | CV-M-L-S | Conside-type |
        # First time appear  - Y/N
        # | Count for this type
        main_information = ['Session id', 'Participant id', 'Start Time - ms',
                            'End Time - ms', 'Duration - ms',
                            'CV - merge - M - L - S', 'concise merge type',
                            'Count for the same utterance type',
                            'First time appear',
                            ]
        # 6373 +9 = 6381

        final_index = main_information + feature_name_list
        save_name = self.session_id + '-' + '200ms-duration-merge' + '.csv'

        print("Start transforming")
        print("The lenngth of index" + str(len(final_index)))
        # list of list Bug here
        oneRow = self.final_merge_list[0]
        # debug here
        print("---------")
        #  print(self.final_merge_list)
        # The lenngth of index6381

        print("The lenngth of one row" + str(len(oneRow)))
        current_df = pd.DataFrame(self.final_merge_list, columns=final_index)
        current_df.to_csv(save_name)

    '''
    As the last time in the opensimile is not equal to the last time in 
    the eaf
    
    we need to find the neast value between last time in the eaf and last time
    in the opensimle
    And delete the row after that
        input is the current speaker dataframe 
        return top10 
        
        @input ccurrent_speaker df 
    '''

    def data_preprocessing_for_current_spaker(self, current_df):
        segment__end_time_list = current_df[['end time-ms']].values.tolist()

        top10_segment_value_list = self.myUtility_function.flatten_list_of_lists(
            self.myUtility_function.find_top_10_max_values(segment__end_time_list))

        maxValue_in_eaf = self.max_time_inference_dataset
        # Min value in the list has lowest absolute value between maxValue_in_eaf
        min_value_absolute = self.myUtility_function.find_closest_value(top10_segment_value_list, maxValue_in_eaf)

        final_df = self.myUtility_function.calibrate_dataframe(current_df, min_value_absolute)

        return final_df

    def acquire_max_time_inference_dataset(self):
        inference_df = pd.read_csv(self.inference_path)
        inference__end_time_list = inference_df[['End Time-ms']].values.tolist()
        inference_max = max(inference__end_time_list)[0]
        self.max_time_inference_dataset = inference_max

    def acquire_paricpant_list(self):

        # load this csv into pandas
        inference_current_session_df = pd.read_csv(self.inference_path)
        # Acquire the list of name
        participant_ids_collection = inference_current_session_df['Participant id'].tolist()
        participant_id_list = list(set(participant_ids_collection))
        self.particpiant_list = participant_id_list

    def opensimile_address_function(self):
        currentDirct: str = os.getcwd()
        specificSession_number = self.session_number
        formate_number = self.myUtility_function.format_number(specificSession_number)
        file_name = 'S' + formate_number + '-segmentedby200ms.csv'

        final_dirctory = currentDirct + '/Duration_opensimile_dataset/' + file_name
        self.openSimile_csv_path = final_dirctory

    def inference_csv_addres_function(self):
        currentDirct: str = os.getcwd()
        specificSession_number = self.session_number
        formate_number = self.myUtility_function.format_number(specificSession_number)
        file_name = 'S' + formate_number + '-inference.csv'
        final_dirctory = currentDirct + '/inference_dataset/' + file_name
        self.inference_path = final_dirctory

    # Main list is list of list
    # target sublist is main_list
    def find_sublist_index(self, main_list, target_sublist):
        for index, sublist in enumerate(main_list):
            if sublist == target_sublist:
                return index
        return -1

    # Compute the threshold dynamically
    def threshold_selection_algorithm(self, list1: list[str], list2: list[str]):
        # Calculate distance
        distances = []

        for range1 in list1:
            in_range = False
            for range2 in list2:
                if range1[0] >= range2[0] and range1[1] <= range2[1]:
                    in_range = True
                    break

            if not in_range:
                nearest_end_distance = min(abs(range1[1] - range2[1]) for range2 in list2)
                distances.append(nearest_end_distance)

        unique_numbers = list(set(distances))
        sort_distance = sorted(unique_numbers)

        median_in_list: int = int(median(sort_distance))

        threshold: int = int(median_in_list)

        return threshold

    # Need to interate twice

    '''
     def inner_case_One(self,start_time_in_open_simile,
                           start_time_list_2_ELAN,ent_time_in_open_simile,end_time_list_2_ELAN,
                           current_particpiant,currenrt_id,current_speaker_inELAN_df,current_speaker_OpenSimle_df
                           ,current_threshold):
    '''

    def merge_single_session(self):

        # Initilization
        ELAN_Sample_csv_path = self.inference_path
        Opensimile_sample_csv_path = self.openSimile_csv_path
        sessionName =  self.session_id
        print("Current session is "+sessionName)
        print("--------------------\n")
        myTestInstance = MergeAlgo(ELAN_Sample_csv_path, Opensimile_sample_csv_path,sessionName)

        myTestInstance.execute()

        myTestInstance.export_csv()