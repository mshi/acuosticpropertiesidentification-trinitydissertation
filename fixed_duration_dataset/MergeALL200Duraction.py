import os
import glob
import os
import pandas as pd

from tqdm import tqdm


def get_final_index():
    file_paths = 'feature_name_list_compare.txt'
    filename = file_paths

    content_list = []

    with open(filename, 'r') as file:
        content_list = [line.strip() for line in file]

    feature_name_list = content_list
    # the feature list
    # Session id,Participant id,Start Time-ms,End Time-ms,Duration-ms,CV-merge-M-L-S,concise merge type
    basic_info = ['Session id', 'Participant id', 'Start Time - ms',
                        'End Time - ms', 'Duration - ms',
                        'CV - merge - M - L - S', 'concise merge type',
                        'First time appear',
                        'Count for the same utterance type',
                        ]
    finalIndex = basic_info + feature_name_list

    return finalIndex

def execute():

    final_directory = '/Users/mingshi/Desktop/PythonProject/Range_alignment/Merge200_Duration'

    # List to store all the list of lists
    all_lists = []
    posfix = '-200ms-duration-merge.csv'
    # file_count = sum(1 for filename in os.listdir(final_directory) if filename.endswith(".csv"))
    file_count = 18
    session_id_list = ['S02', 'S03', 'S04', 'S05', 'S07', 'S08', 'S09', 'S10', 'S11',
                       'S13', 'S14', 'S17', 'S18', 'S19', 'S20', 'S21', 'S22', 'S23']

    with tqdm(total=file_count, desc='Processing 18 CSVs') as pbar:
        for digits in session_id_list:
            file_path = final_directory + '/' + digits + posfix
            print(file_path)
            data = pd.read_csv(file_path)

            # Need to drop the first column here
            data.drop(columns=data.columns[0], axis=1, inplace=True)
            list_of_lists = data.values.tolist()
            all_lists.extend(list_of_lists)
            pbar.update(1)  #

    finalIndex =  get_final_index()

    progressive_all_lists = tqdm(all_lists)


    final_intermiddate_df = pd.DataFrame(progressive_all_lists, columns=finalIndex)


    final_intermidate_save_name = 'final-merge-all-200ms-firstpass.csv'

    # Open a file in write mode
    with open(final_intermidate_save_name, 'w', newline='', encoding='utf-8') as file:
        # Write header
        file.write(final_intermiddate_df.to_csv(index=False, header=True, line_terminator='\n'))

        # Iterate over DataFrame rows with a progress bar
        for row in tqdm(final_intermiddate_df.itertuples(index=False), total=len(final_intermiddate_df)):
            # Write each row
            file.write(','.join(str(item) for item in row) + '\n')


    #



if __name__ == '__main__':
    execute()