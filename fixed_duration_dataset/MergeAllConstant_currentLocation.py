import csv
import os
import glob
import os
import pandas as pd

from tqdm import tqdm


def get_final_index():
    file_paths = 'feature_name_list_compare.txt'
    filename = file_paths

    content_list = []

    with open(filename, 'r') as file:
        content_list = [line.strip() for line in file]

    feature_name_list = content_list
    # the feature list
    # Session id,Participant id,Start Time-ms,End Time-ms,Duration-ms,CV-merge-M-L-S,concise merge type
    basic_info =  ['Session id', 'Participant id', 'Start Time - ms',
                            'End Time - ms', 'Duration - ms',
                            'CV - merge - M - L - S', 'concise merge type',
                            'Count for the same utterance type',
                            'First time appear',
                            ]
    finalIndex = basic_info + feature_name_list

    return finalIndex

def execute():
    currentDirct: str = os.getcwd()+'/Merge200_Duration/'

    # List to store all the list of lists
    all_lists = []
    posfix = '-merge-constant.csv'
    # file_count = sum(1 for filename in os.listdir(final_directory) if filename.endswith(".csv"))
    file_count = 18
    session_id_list = ['S02', 'S03', 'S04', 'S05', 'S07', 'S08', 'S09', 'S10', 'S11',
                       'S13', 'S14', 'S17', 'S18', 'S19', 'S20', 'S21', 'S22', 'S23']

    with tqdm(total=file_count, desc='Processing 18 CSVs') as pbar:
        for digits in session_id_list:
            file_path = currentDirct+digits + posfix
            print(file_path)
            data = pd.read_csv(file_path)

            # Need to drop the first column here
           # data.drop(columns=data.columns[0], axis=1, inplace=True)
            list_of_lists = data.values.tolist()
            all_lists.extend(list_of_lists)
            pbar.update(1)  #

    finalIndex =  get_final_index()
    all_lists.insert(0, finalIndex)




    final_intermidate_save_name = 'final-merge-all-200ms-firstpass.csv'

    with open(final_intermidate_save_name, mode='w', newline='') as file:
        writer = csv.writer(file)

        # Wrap your loop with tqdm for the progress bar
        for row in tqdm(all_lists, desc="Writing rows"):
            writer.writerow(row)



if __name__ == '__main__':
    execute()