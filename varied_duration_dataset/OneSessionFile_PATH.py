import glob
import os
import pandas as pd

# this class could get the absolute address of csv and wav
class OneSessionFile_PATH:
    def __init__(self,sessionNumber):
        self.sessionNumber = sessionNumber
        self.csv_path:str =''
        self.audio_path_List:list[str] = []
        self.run()

    def get_csv_path(self)->str:
        return self.csv_path

    def get_audio_path_list(self)->list[str]:
        return self.audio_path_List
    def run(self):
        # acquired csv
        currentDirct: str = os.getcwd()
        # enter the director
        specificSession_number = self.sessionNumber
        final_dirctory = currentDirct + "/finalUsedDatasetFirstPass" + "/S" + str(specificSession_number)
        # add another specific Dirctor
        fullDirect_name = final_dirctory

        # List all files in the directory
        files_in_directory = os.listdir(final_dirctory)

        # Filter .csv files
        csv_files_list = [file for file in files_in_directory if file.endswith(".csv")]
        myCSV_path = final_dirctory + "/" + csv_files_list[0]
        self.csv_path = myCSV_path
       # print("current csv path is "+myCSV_path)

        # get wav
        # Filter .wav files
        wav_files = [file for file in files_in_directory if file.endswith(".wav")]

        # Print the names of the WAV files
        print("WAV Files in Current Directory:")
        final_wavFile_list:list[str] =[]
        for wav_file in wav_files:
            temp_path = final_dirctory + "/" + wav_file
           # print(temp_path)
            final_wavFile_list.append(temp_path)

        self.audio_path_List = final_wavFile_list

if __name__ == '__main__':
    SessionId = 3
    mySess = OneSessionFile_PATH(sessionNumber=SessionId)
    print(mySess.get_csv_path())

    print(mySess.audio_path_List)
