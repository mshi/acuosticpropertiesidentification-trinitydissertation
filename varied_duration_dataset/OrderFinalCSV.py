import pandas as pd


file_path = 'final-merge-all.csv'
df = pd.read_csv(file_path)


session_id_list = ['S02', 'S03', 'S04', 'S05', 'S07', 'S08', 'S09', 'S10', 'S11',
                   'S13', 'S14', 'S17', 'S18', 'S19', 'S20', 'S21', 'S22', 'S23']


df['Session id'] = pd.Categorical(df['Session id'], categories=session_id_list, ordered=True)




output_file_path = 'final-merge-all-first-pass.csv'
df.to_csv(output_file_path, index=False)
