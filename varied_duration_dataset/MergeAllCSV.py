import os
import glob
import os
import pandas as pd
from MergeListofList import MyListOfList
from tqdm import tqdm

# In this class we need to load the finalCSV
class MergeAllCSV:
    def __init__(self):
        self.final_index_column: list[str] = []
        self.get_final_index()
        self.csv_collection_path: list[str] = []


    def get_csv_list(self):
        #  currentDirct: str = os.getcwd()
        # enter the director

        final_directory = '/Users/mingshi/Desktop/PythonProject/pythonProject1/final18CSV'

        fullDirect_name_absolute = final_directory
        # get all csv from list
        # Filter .csv files

        csv_files = glob.glob(final_directory + '/*.csv')



        CSV_COLLECTION: list[str] = []
        for csv_file in csv_files:
            temp_path = final_directory + "/" + csv_file
            print(csv_file)
            CSV_COLLECTION.append(temp_path)

        self.csv_collection_path = CSV_COLLECTION

    def execute2(self):


        final_directory = '/Users/mingshi/Desktop/PythonProject/pythonProject1/final18CSV'

        # List to store all the list of lists
        all_lists = []
        posfix = '-final.csv'
       # file_count = sum(1 for filename in os.listdir(final_directory) if filename.endswith(".csv"))
        file_count= 18
        session_id_list = ['S02', 'S03', 'S04', 'S05', 'S07', 'S08', 'S09', 'S10', 'S11',
                           'S13', 'S14', 'S17', 'S18', 'S19', 'S20', 'S21', 'S22', 'S23']

        with tqdm(total=file_count, desc='Processing CSVs') as pbar:

            for digits in session_id_list:

                file_path = final_directory+'/'+digits+posfix
                print(file_path)
                data = pd.read_csv(file_path)
                list_of_lists = data.values.tolist()
                all_lists.extend(list_of_lists)
                pbar.update(1)  #

        finalIndex = self.final_index_column
        # Transform list of list
        final_intermiddate_df = pd.DataFrame(all_lists, columns=finalIndex)
        final_intermidate_save_name = 'final-merge-all-first-pass-intermidate.csv'
        final_intermiddate_df.to_csv(final_intermidate_save_name)

        #
        print("Drop the first column")
        # reload again to remove the first column

        print("Genaeate the final one")
        final_name = "final-merge-all-first-pass.csv"

        final_df = pd.read_csv(final_intermidate_save_name)
        final_df.drop(columns=final_df.columns[0], axis=1, inplace=True)
        final_df.to_csv(final_name, index=False)

        # Displaying merged list of lists
       # print("Merged List of Lists:")
       # print(all_lists)


    def get_final_index(self) -> str:
        file_paths = 'feature_name_list_compare.txt'
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]

        feature_name_list = content_list
        # the feature list
        # Session id,Participant id,Start Time-ms,End Time-ms,Duration-ms,CV-merge-M-L-S,concise merge type
        basic_info = [
            'Session id', 'Participant id', 'Start Time - ms',
            'End Time - ms', 'Duration - ms', 'CV - merge - M - L - S',
            'concise merge type'
        ]
        finalIndex = basic_info + feature_name_list

        self.final_index_column = finalIndex


    def execute(self):
        my_csv_list = self.csv_collection_path
        merger_listOFList_container = MyListOfList()

        for current_csv in my_csv_list:
            my_current_path = current_csv
            current_df = pd.read_csv(my_current_path)
            temp_listOfList = current_df.values.tolist()
            merger_listOFList_container.append_list_of_list(temp_listOfList)
            print("Finished the current iteration")

        print("Finished all")
        # Merge all
        print("Start mergeing")
        merger_listOFList_container.merge_all()

        final_List = merger_listOFList_container.get_final_listOfList()
        finalIndex = self.final_index_column
        # Transform list of list
        final_intermiddate_df = pd.DataFrame(final_List, columns=finalIndex)
        final_intermidate_save_name = 'final-merge-all-intermidate.csv'
        final_intermiddate_df.to_csv(final_intermidate_save_name)

        #
        print("Drop the first column")
        # reload again to remove the first column

        print("Genaeate the final one")
        final_name = "final-merge-all-first-pass.csv"

        final_df = pd.read_csv(final_intermidate_save_name)
        final_df.drop(columns=final_df.columns[0], axis=1, inplace=True)
        final_df.to_csv(final_name, index=False)


if __name__ == '__main__':
    myMergeAll = MergeAllCSV()
    myMergeAll.execute2()
