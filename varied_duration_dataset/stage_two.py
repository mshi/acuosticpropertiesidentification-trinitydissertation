
import pandas as pd
import os
import glob

from inference import my_inderence


# The input is 18 datasets generated from phase 1

class stage_two_dataset:
    def __init__(self):

        self.inital_csv_collection= None

        self.load_all_csv_from_inital_dataset_algorithm()

        self.processing_file()


    def load_all_csv_from_inital_dataset_algorithm(self):
        current_directory = os.getcwd()
        csv_files = glob.glob(os.path.join(current_directory, '*.csv'))
        self.inital_csv_collection = csv_files

    def processing_file(self):
        csv_collection = self.inital_csv_collection

        index_column = ['Session id', 'Participant id', 'Start Time-ms', 'End Time-ms', 'Duration-ms',
                        'CV-merge-M-L-S']
        new_index_column = ['Session id', 'Participant id', 'Start Time-ms', 'End Time-ms', 'Duration-ms',
                        'CV-merge-M-L-S','concise merge type']

        folder_name = 'inference'
        for file in csv_collection:
            csv_file_name = file
            current_df  = pd.read_csv(csv_file_name)
            current_session_code:str =''
            current_df.drop(columns=current_df.columns[0], axis=1, inplace=True)
            current_list_of_list =[]
            #iterate the current df
            for index, row in current_df.iterrows():
                session_id:str = row[index_column[0]]
                current_session_code = session_id

                participant_id:str = row[index_column[1]]

                start_time:str= row[index_column[2]]

                end_time:str = row[index_column[3]]

                duration_time:str = row[index_column[4]]

                cv_merge:str= row[index_column[5]]
                #new column generated from inference

                myInference = my_inderence(cv_merge)
                new_concise_type = myInference.inference_classification()

                current_list = [session_id,participant_id,start_time
                                ,end_time,duration_time,cv_merge,new_concise_type]
                current_list_of_list.append(current_list)

            filename_output = current_session_code + "-inference" + ".csv"
            new_session_pd = pd.DataFrame(current_list_of_list, columns=new_index_column)

            if not os.path.exists(folder_name):
                os.makedirs(folder_name)

            file_path = os.path.join(folder_name, filename_output)
            new_session_pd.to_csv(file_path, index=False)







if __name__ == '__main__':

    myStageTwo = stage_two_dataset()

 #   print(stage_two_dataset.print_csv_collection())