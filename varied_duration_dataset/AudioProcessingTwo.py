import glob
import os
import pandas as pd
import opensmile
from paricipantCodeChunk import paricipantCodeChunk
import audiosegment
# The final one
import time

class AudioProcessingTwo:
    def __init__(self,input_csv,input_mono_audio):
        self.input_csv:str = input_csv
        self.input_mono_audio:str  = input_mono_audio
        self.final_datalist = []

    def run(self):
        #Configureation
        csv_file_name =  self.input_csv
        current_df = pd.read_csv(csv_file_name)
        audofilepath: str = self.input_mono_audio
        input_audio_file = audiosegment.from_file(audofilepath)

        #The filter one
        #for paricipant code extraction
        # first extract the root file name
        root_dire_file_name = audofilepath.rsplit('/', 1)[-1]
#--------------
        # Here we replace using a fife
        myChunk = paricipantCodeChunk(root_dire_file_name)
        returnparticiant_code = myChunk.get_final_particpant_code()
#---------------
        filtered_df = current_df[current_df['Participant id']==returnparticiant_code]


        audofilepath: str = self.input_mono_audio
        input_audio_file = audiosegment.from_file(audofilepath)

# Filter_df is the specific speaker in the particular session
        for index, row in filtered_df.iterrows():
            print("-------------")

            selected_columns_start = row.iloc[2]

            # print(type(selected_columns_start))
            selected_columns_end = row.iloc[3]
            startTime:int = int(selected_columns_start)
            endTime:int= int(selected_columns_end)
            startTime:int = int(selected_columns_start)

            endTime:int = int(selected_columns_end)
            print(index)
            print(startTime)
            print(endTime)
            # Start segement
            current_audio_segment = input_audio_file[startTime:endTime]
            # outfilename = folder + '-' + 'discourse' + '-' + str(index) + '.wav'
            current_seesion_id = row.iloc[0]
            p_d = row.iloc[1]
            conversation_type_s = row.iloc[5]
            hypen = '-'
            file_post_fix = '.wav'
            # outfilename_aidio = current_seesion_id + '-' + p_d + '-' + conversation_type_s +'-'+startTime+'+'.wav'
            outfilename_audio = current_seesion_id + hypen + p_d + hypen + str(startTime) + hypen + str(
                endTime) + hypen + conversation_type_s + file_post_fix

            current_audio_segment.export(outfilename_audio, format="wav")
            print(f"Audio segment saved to {outfilename_audio}")
            # Loading already
            print("Staring using Oensimile")

            smile = opensmile.Smile(
                feature_set=opensmile.FeatureSet.ComParE_2016,
                feature_level=opensmile.FeatureLevel.Functionals,
            )
            start_time_counter = time.perf_counter()

            result_df_current = smile.process_file(outfilename_audio)

            end_time_counter = time.perf_counter()
            runtime = end_time_counter - start_time_counter
            print(f"The line of code took {runtime} seconds to run.")

            dfcurrent_T = result_df_current.T

            dfcurrent_T.reset_index(inplace=True)

            list_of_listssss = dfcurrent_T.values.tolist()

            feature_value_list = []

            # Iterate through the list of lists
            for inner_list in list_of_listssss:
                # feature_name_list.append(inner_list[0])  # Add the first element to the first list

                feature_value_list.append(inner_list[1])  # Add the second element to the second list
            #

            original_row_value_List = row.tolist()

            merge_value = original_row_value_List + feature_value_list

            self.final_datalist.append(merge_value)

# Deleted
            wav_files = glob.glob('*.wav')
            # Remove current
            for wav_file in wav_files:
                try:
                    os.remove(wav_file)
                    print(f"Deleted {wav_file}")
                except OSError as e:
                    print(f"Error: {wav_file} : {e.strerror}")

            print("-------------")

    def get_final_list(self):
        return self.final_datalist

    def export_final_csv(self):

        file_paths = 'feature_name_list_compare.txt'
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]

        feature_name_list = content_list
        # the feature list
        #Session id,Participant id,Start Time-ms,End Time-ms,Duration-ms,CV-merge-M-L-S,concise merge type
        basic_info = [
            'Session id', 'Participant id', 'Start Time - ms',
            'End Time - ms', 'Duration - ms', 'CV - merge - M - L - S',
            'concise merge type'
        ]
        finalIndex = basic_info + feature_name_list
        # list of list
        final_List = self.final_datalist

        #current_df = pd.DataFrame(final_List, columns=finalIndex)

       # current_df.to_csv("session2_moderator-continueous.csv")

       # return current_df

if __name__ == '__main__':
    audio_file_path:str = '/Users/mingshi/Desktop/DissertationProject/Dataset/MONO/S2/M001_S02_audio_M_C.wav'
    input_csv_addres:str = '/Users/mingshi/Desktop/PythonProject/pythonProject1/inference/S02-inference.csv'
    myAudioS = AudioProcessingTwo(input_csv_addres,audio_file_path)
    #input_audio_file = audiosegment.from_file(file_path)
    #current_audio_segment = input_audio_file[0:100]

    print("sss")
    myAudioS.run()
    myAudioS.export_final_csv()
