
# This class is to chunk the participant code from the audio

class paricipantCodeChunk:
    def __init__(self,input_audio_file):
        self.input_audio_file:str = input_audio_file
        self.returnCode:str = ''
        self.processing()

#  return is a string
    def processing(self)->str:

        audio_file_path = self.input_audio_file

        #Remove the extension
        file_name_without_extension:str = audio_file_path.replace(".wav", "")

#       Case 1
        if file_name_without_extension.startswith("M"):

            selected_string_for_moderator = file_name_without_extension[:8]
            self.returnCode = selected_string_for_moderator.strip()
            return self.returnCode

        #another case

        if file_name_without_extension.startswith("P"):

            split_string_list = file_name_without_extension.split('_')
            participant_code = split_string_list[0]
            final_paritcipant_code = participant_code

            #print(length_paricipaiantCode)
            corrected_button: bool = False
            correct_paritcipantCode = ""

            # Check the length and verify
            # check the size of the partiipant code

            if len(participant_code) == 5 and "0" in participant_code:
                correct_paritcipantCode = participant_code.replace("0", "", 1)
                corrected_button = True
            # print(correct_paritcipantCode)

            if corrected_button == True:
                final_paritcipant_code = correct_paritcipantCode

            self.returnCode = final_paritcipant_code.strip()
            return self.returnCode


# get this string
    def get_final_particpant_code(self)->str:
        return self.returnCode


if __name__ == '__main__':
    audio_file_path = 'M003_S23_audio_M_C.wav'
    myChunk = paricipantCodeChunk(audio_file_path)
    returnStr = myChunk.get_final_particpant_code()
    print(len(returnStr))