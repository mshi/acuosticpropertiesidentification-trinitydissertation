import glob
import os
import pandas as pd
import opensmile
import random
import audiosegment
from pydub import AudioSegment
from tqdm import tqdm
import time


class AudioSegment:
    def __init__(self,audio_path):
        self.audio_path:str = audio_path
        self.final_mergeList= []
        self.segmentTuple =[]
        self.acquire_audio_segment_sequence()

    def acquire_audio_segment_sequence(self):
        myfile_path = self.audio_path
        audio = audiosegment.from_file(myfile_path)

        # set 100 mileseconds
        # as the total audio length is 622822 is great than the eaf end time 622817

        # This fine grained level as 622822
        segment_duration = 100  # 100 milliseconds
        audolength = len(audio)
        # List to store audio segments
        segmentsTuplist = []

        # Iterate through the audio file and segment it
        for i in range(0, audolength, segment_duration):
            start_time = i
            end_time = i + segment_duration
            print(start_time)
            print(end_time)
            tuple_temp = (start_time, end_time)
            print("_--------------")
            segmentsTuplist.append(tuple_temp)

        self.segmentTuple = segmentsTuplist
    def segment_identical_moment_audio(self):
        # Define marco
        FIXED_DUARATION:int = 100
        #load into the audioSegment file
        audofilepath:str = self.audio_path
        input_audio_file =audiosegment.from_file(audofilepath)
####################################

        for eachTuple in self.segmentTuple:

            startTime: int = eachTuple[0]
            endTime: int = eachTuple[1]
            #start segment
            current_audio_segment = input_audio_file[startTime:endTime]

            # outfilename = folder + '-' + 'discourse' + '-' + str(index) + '.wav'

            hypen = '-'
            file_post_fix = '.wav'
            # outfilename_aidio = current_seesion_id + '-' + p_d + '-' + conversation_type_s +'-'+startTime+'+'.wav'
            outfilename_audio = str(startTime) + hypen + str(
                endTime) +  file_post_fix

            current_audio_segment.export(outfilename_audio, format="wav")
            print(f"Audio segment saved to {outfilename_audio}")
####################################
            # Loading already
            print("Staring using Oensimile")

            smile = opensmile.Smile(
                feature_set=opensmile.FeatureSet.ComParE_2016,
                feature_level=opensmile.FeatureLevel.Functionals,
            )
            start_time_counter = time.perf_counter()

            result_df_current = smile.process_file(outfilename_audio)


            end_time_counter = time.perf_counter()
            runtime = end_time_counter - start_time_counter
            print(f"The line of code took {runtime} seconds to run.")

            dfcurrent_T = result_df_current.T

            dfcurrent_T.reset_index(inplace=True)

            list_of_listssss = dfcurrent_T.values.tolist()

            feature_value_list = []

            # Iterate through the list of lists
            for inner_list in list_of_listssss:
                # feature_name_list.append(inner_list[0])  # Add the first element to the first list

                feature_value_list.append(inner_list[1])  # Add the second element to the second list
            #
            # start time | end time | duration | Acuostic feature
            inforation_list = [startTime,endTime,FIXED_DUARATION]
            curr_merge_list =inforation_list+ feature_value_list
            self.final_mergeList.append(curr_merge_list)
            # Delte file name

            wav_files = glob.glob('*.wav')
            # Remove current
            for wav_file in wav_files:
                try:
                    os.remove(wav_file)
                    print(f"Deleted {wav_file}")
                except OSError as e:
                    print(f"Error: {wav_file} : {e.strerror}")

            print("-------------")

    def export_final_csv(self):

        file_paths = 'feature_name_list_compare.txt'
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]

        feature_name_list = content_list
        # add origianl one
        basic_info = [
            'start time-ms', 'end time-ms', 'duration-ms'
        ]

        finalIndex = basic_info+feature_name_list
        # list of list
        final_List = self.final_mergeList

        current_df = pd.DataFrame(final_List, columns=finalIndex)

        current_df.to_csv("session2-moderator-audio-segment-by-opensimile-100Ms-gap.csv")

        return current_df


if __name__ == '__main__':
    file_path:str = '/Users/mingshi/Desktop/DissertationProject/Dataset/MONO/S2/M001_S02_audio_M_C.wav'

    myAudioS = AudioSegment(file_path)
    #input_audio_file = audiosegment.from_file(file_path)
    #current_audio_segment = input_audio_file[0:100]

    print("sss")
    myAudioS.segment_identical_moment_audio()
    myAudioS.export_final_csv()