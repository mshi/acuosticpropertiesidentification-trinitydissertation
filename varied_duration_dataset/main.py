import os
import glob

def find_csv_files_in_current_directory() -> list[str]:

    current_directory = os.getcwd()  # Get the current directory
    csv_files = glob.glob(os.path.join(current_directory, '*.csv'))  # List all CSV files
    return csv_files

# Example usage
csv_files = find_csv_files_in_current_directory()
for file in csv_files:
    print(file)