import glob
import os
import pandas as pd
from OneSessionFile_PATH import OneSessionFile_PATH
#This class intend to merge one cession csv
from AudioProcessingTwo import AudioProcessingTwo
from MergeListofList import MyListOfList
class MergeOneSession:
    def __init__(self,sessionNumber):
        self.sessionNumber = sessionNumber
        path_getter = OneSessionFile_PATH(sessionNumber=self.sessionNumber)
        self.myCSVpath = path_getter.get_csv_path()
        self.my_audio_path_collection = path_getter.get_audio_path_list()
        self.merger_listOFList_container = MyListOfList()
        self.final_index_column:list[str] = []
        self.sessionID = ''
        self.get_sessionID()
        self.get_final_index()
    def get_sessionID(self):
        # /Users/mingshi/Desktop/PythonProject/pythonProject1/finalUsedDatasetFirstPass/S3/S03-inference.csv
        current_csv_path = self.myCSVpath
# The last name
        #S03-inference.csv
        root_dire_CSV_file_name = current_csv_path.rsplit('/', 1)[-1]

        file_name_without_extension = root_dire_CSV_file_name.replace(".csv", "")
#       GET S01
        split_string_list = file_name_without_extension.split('-')
        SESSIONID = split_string_list[0]

        self.sessionID = SESSIONID



    def get_final_index(self)->str:
        file_paths = 'feature_name_list_compare.txt'
        filename = file_paths

        content_list = []

        with open(filename, 'r') as file:
            content_list = [line.strip() for line in file]

        feature_name_list = content_list
        # the feature list
        # Session id,Participant id,Start Time-ms,End Time-ms,Duration-ms,CV-merge-M-L-S,concise merge type
        basic_info = [
            'Session id', 'Participant id', 'Start Time - ms',
            'End Time - ms', 'Duration - ms', 'CV - merge - M - L - S',
            'concise merge type'
        ]
        finalIndex = basic_info + feature_name_list

        self.final_index_column = finalIndex



    def execute(self):

        current_csvFile = self.myCSVpath
        current_audio_list = self.my_audio_path_collection

        for current_audio_path in current_audio_list:
            myAudio_merge_temp = AudioProcessingTwo(current_csvFile, current_audio_path)
            myAudio_merge_temp.run()
            temp_list_of_list = myAudio_merge_temp.get_final_list()
            self.merger_listOFList_container.append_list_of_list(temp_list_of_list)
        print("Finishing adding")
        #Final all
        #mergeall
        # Merge all lists
        print("Start merging")
        self.merger_listOFList_container.merge_all()
        final_List =self.merger_listOFList_container.get_final_listOfList()

        #filename
        session_name = self.sessionID
        print("Save intemdiate file")
        intermeddiate_name = session_name+"-interim.csv"
        finalIndex = self.final_index_column

        # Transform list of list into pandas
        current_df = pd.DataFrame(final_List, columns=finalIndex)
        # Save the interi

        current_df.to_csv(intermeddiate_name)
        #reload again to remove the first column
        print("Genaeate the final one")
        final_name = session_name + "-final.csv"
        final_df = pd.read_csv(intermeddiate_name)
        final_df.drop(columns=final_df.columns[0], axis=1, inplace=True)
        final_df.to_csv(final_name, index=False)



if __name__ == '__main__':
    session_id_list = [2, 3, 4, 5, 7, 8, 9, 10, 11, 13, 14, 17, 18, 19, 20, 21, 22, 23]

    for currenrt_id in session_id_list:
        specificSession_number:int =currenrt_id
        print('Current session is '+str(specificSession_number))
        my_mergeFuncion = MergeOneSession(sessionNumber=specificSession_number)
        my_mergeFuncion.execute()
        print("+++++++++++++++++++++++++++++++")
        print("Finished one ")

