class MyListOfList:

    def __init__(self):
        self.data = []

    def append_list_of_list(self, lst):
        self.data.append(lst)

    def merge_all(self):

        merged_list = [item for sublist in self.data for item in sublist]
        return merged_list


    def add_index_front_of_list(self,index_list):
        self.data.insert(0, index_list)


manager = MyListOfList()


