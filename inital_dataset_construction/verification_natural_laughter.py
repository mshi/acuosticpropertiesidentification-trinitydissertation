import re
class verification_to_natural_laughter:
    def __init__(self, input_str_vocoalSequence:str,specific_laughter_tag:str):
        self.specific_laughter_tag=specific_laughter_tag
        self.input_str_vocoalSequence = input_str_vocoalSequence
        self.return_str = self.process()

    def get_return_str(self)->str:
        return self.return_str

    def process(self)->str:

        input_str:str= self.input_str_vocoalSequence
# [[laugh] V]
        word_List: list[str] = input_str.split()
        new_list: list[str] = []
        laughter_marco: str = ' [laugh]'.strip()
        punctuations_symbols: list[str] = ['!', '?', '.', ';', ',']
        # [laugh]


        #split puncation
        # processing wordlist
        for current_str in word_List:

            # judge
            puncationSymbolJudge =  any(symbol in current_str for symbol in punctuations_symbols)


            if puncationSymbolJudge == True:
                #split the current string
                part1,part2 = self.split_on_first_punctuation(current_str,punctuations_symbols)
                new_list.append(part1)
                new_list.append(part2)
            else:
                # jduge laugher or normal V
                if current_str == laughter_marco:
                    hypen = '-'
                    mergelaughter = current_str +hypen+self.specific_laughter_tag
                    new_list.append(mergelaughter)
                else:
                    new_list.append(current_str)
        #post processing
        # add new
        return_str = ''
        size_of_list_store = len(new_list)
        if size_of_list_store == 1:
            return_str = new_list[0]
        else:
            for index_list, curr_str in enumerate(new_list):

                if index_list != (size_of_list_store - 1):
                    # discuss the case
                    if index_list == 0:
                        return_str += curr_str

                    else:
                        # (0,lastOne)
                        # if thie one is not puncation add spaces
                        # if not do not add
                        if curr_str in punctuations_symbols:
                            return_str += curr_str
                        else:
                            return_str += ' '
                            return_str += curr_str

                else:
                    # Lastone
                    # judge symbol
                    if curr_str in punctuations_symbols:

                        return_str += curr_str
                    else:
                        return_str += ' '
                        return_str += curr_str

        return return_str




    def split_on_first_punctuation(self,s: str, punctuations: list) -> tuple:

        pattern = '[' + re.escape(''.join(punctuations)) + ']'


        match = re.search(pattern, s)
        if match:
            index = match.start()
            return s[:index], s[index:]
        else:
            return s, None

if __name__ == '__main__':
    vocal_str = '[laugh] V.'
    speicial_laghter='Mirthful'
    myVocal = verification_to_natural_laughter(vocal_str,speicial_laghter)
    returnsss = myVocal.process()

    print("Before processsing ：" + vocal_str)
    print("After processing  ：" + returnsss)
 # vocal_str = '[laugh] V.'
  #  myVocal = verification_to_natural_laughter(vocal_str)
  #  punctuations_symbols = ['!', '?', '.', ';', ',']
 #   input_str = "V."
 #   part1, part2 =myVocal.split_on_first_punctuation(input_str, punctuations_symbols)
##    print(part1)
#    print(part2)
   # vocal_str = '[laugh] V.'
 #   myVocal = verification_to_natural_laughter(vocal_str)
#    myVocal.process()
