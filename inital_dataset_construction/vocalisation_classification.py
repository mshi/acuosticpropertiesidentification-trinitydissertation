
import re


class Classfication_funtion:
    def __init__(self,input_str:str):
        self.input_str =input_str
        self.returnStr =self.vtags_sequence_transformation()


    def get_return(self)->str:
        return self.returnStr
    def is_enclosed_in_brackets(self,input_str: str) -> bool:
        return input_str.startswith('[') and input_str.endswith(']')




    # First step utterance to -> [V] [Laugh] V
    # MAINTAIN  [laugh]

    #split the puncation on originalist
    def split_on_punctuation(self,strings_list: list) -> list:
        result = []

        punctuation_pattern = r'[^\w\s]'

        for s in strings_list:
            if re.search(punctuation_pattern, s):

                match = re.search(punctuation_pattern, s)
                index = match.start()

                # Split the string into two parts
                part1 = s[:index]
                part2 = s[index:]


                result.append(part1)
                result.append(part2)
            else:

                result.append(s)

        return result

    def remove_hyphens(self,strings_list: list) -> list:
        result = [s.replace('-', '') for s in strings_list]
        return result

    def vtags_sequence_transformation(self)->str:
        # receive the augment text
        # ---------------------
        # Initilization
        # 1 Transform into Lowercase
        input_str = self.input_str
        return_str: str = ''
        lowercase_input: str = input_str.lower()
        # Transform from str to list<str>
        #
        word_List:list[str] = lowercase_input.split()
        new_list:list[str] =[]
        # for s in strings_list:

        # processing wordlist
        for current_str in word_List:
            judge_bracket:bool = self.is_enclosed_in_brackets(current_str)

            if judge_bracket == True:
                #remove spacess in brack

                remove_augment_text:str = self.remove_spaces_in_brackets(current_str)
                new_list.append(remove_augment_text)

            else:
                processing_str= ''
                hypen_in_conditnion :bool = '-' in current_str
                if hypen_in_conditnion == True:
                    processing_str=current_str.replace('-', '')
                else:
                    processing_str = current_str

                punctuation_pattern = r'[^\w\s]'

                if re.search(punctuation_pattern, processing_str):
                    match = re.search(punctuation_pattern, processing_str)
                    index = match.start()

                    # Split the string into two parts
                    part1 = processing_str[:index]
                    part2 = processing_str[index:]

                    new_list.append(part1)
                    new_list.append(part2)
                else:
                    new_list.append(processing_str)




        # set laughter marco
        #new_list
        laughter_marco:str = ' [laugh]'.strip()
        normal_vocal_brackert_maro:str = '[V]'.strip()
        normal_vocal__maro:str = 'V '.strip()
        # For results

        store_list: list[str] = []

        # we need to deal with separately.

        punctuations_symbols: list[str] = ['!', '?', '.', ';', ',']
        list_vocal:list[str] =[]

        # Button for previous one is
        # if previous word is V current V is V add  until encounter the [V] to
        # use set (listV) then remove all for this listOFV or the last element is puncation

        lengthOf_newList = len(new_list)
    #---------------------
        # Iterate the word
        for index_forword, wordcurrent in enumerate(new_list):
            # Modify code here
            word_stripVersion: str = wordcurrent.strip()

            # step 1
            # we need to determine whether current element with spquare breacket
            if(self.is_enclosed_in_brackets(word_stripVersion)== True):

                # need to check
                # list_vocal word in the next case
                size_of_list_container:int = len(list_vocal)

                if size_of_list_container >0:
                    store_list.append(normal_vocal__maro)
                    # clear all for list vocal
                    list_vocal.clear()


                #if within  bracket
                # then weed nee to
                wordcurrent_v1 = self.remove_spaces_in_brackets(wordcurrent)
                # determine this one equals to [laughter]
                if wordcurrent_v1 == laughter_marco:
                 # add this one to storelist directly
                 # laughte
                    store_list.append(laughter_marco)
                else:
                    #other case normal_vocal_maro
                    store_list.append(normal_vocal_brackert_maro)


            else:
                # puncation in this list
                # word_stripVersion
                #directly
                # we need to judge case
                # 1 puncation
                # non puncation
                if wordcurrent not in punctuations_symbols:

                    #determine the end of list
                    if index_forword == (lengthOf_newList-1):
                        store_list.append(normal_vocal__maro)

                        # clear all
                        list_vocal.clear()
                    else:
                        list_vocal.append(word_stripVersion)
                else:
                    # in puncation sumbol
                    store_list.append(normal_vocal__maro)
                    store_list.append(wordcurrent)
                    # clear all
                    list_vocal.clear()


      #--------------------------
        # Postprocessing
        # store_list

        size_of_list_store = len(store_list)

        if size_of_list_store == 1:
            return_str = store_list[0]

        else:
            for index_list, curr_str in enumerate(store_list):

                if index_list != (size_of_list_store - 1):
                    # discuss the case
                    if index_list == 0:
                        return_str += curr_str

                    else:
                        #(0,lastOne)
                        # if thie one is not puncation add spaces
                        # if not do not add
                        if curr_str in punctuations_symbols:
                            return_str += curr_str
                        else:
                            return_str += ' '
                            return_str += curr_str

                else:
                    # Lastone
                    # judge symbol
                    if curr_str in punctuations_symbols:

                        return_str += curr_str
                    else:
                        return_str += ' '
                        return_str += curr_str

        return return_str


    # split string into puncation and spaces
    def split_string_and_punctuation(self,input_str: str) -> list:

        pattern = r'\w+|[^\w\s]'
        return re.findall(pattern, input_str)

    #Utility functionality
    def remove_spaces_in_brackets(self,input_str: str) -> str:
        def replace_space(match):
            return match.group(0).replace(' ', '')

        # Regular expression to find text within brackets
        pattern = r'\[.*?\]'
        return re.sub(pattern, replace_space, input_str)

    # ------------------------



if __name__ == '__main__':
    # Download the English model for Stanza

  #  auggment_text:str = text_lemmaprocessing(processing_annontation_text)
   # stanza.download('en')
    processing_annontation_text: str ="[Laugh] Vilon"
  #  auggment_text: str = text_lemmaprocessing(processing_annontation_text)
  # "Woo-hoo!" -> V！  right
# "Hello, this is a test + with [brackets] and other symbols." -》V,V [V] V. eighr
    # "Hello, this's a test + with [brackets] and other symbols." -> V, V [V] V.
   # "[laugh] [ah]"  [laugh] [V]
# [Laugh] Vilon -> Vright
    myClass = Classfication_funtion(processing_annontation_text)

    print("Ater prcossing tags")
    print("---------")
    print(myClass.get_return())
  #  print(list_of_strings)
    #words_list = auggment_text.split(" ")
   # print(words_list)
