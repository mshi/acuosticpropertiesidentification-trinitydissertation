import os
import re
import subprocess


def get_installed_packages():
    try:
        # Run pip freeze to get the list of installed packages
        installed_packages = subprocess.check_output([sys.executable, '-m', 'pip', 'freeze'])
        installed_packages = installed_packages.decode().split('\n')
        return {pkg.split('==')[0]: pkg for pkg in installed_packages if '==' in pkg}
    except subprocess.CalledProcessError as e:
        print(f"An error occurred while getting the installed packages: {e}")
        return {}


def parse_imports(directory):
    imported_packages = set()
    for subdir, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.py'):
                file_path = os.path.join(subdir, file)
                with open(file_path, 'r') as f:
                    contents = f.read()
                    # Find all imported packages
                    for import_statement in re.findall(r'^(?:from|import) (\S+)', contents, re.M):
                        package = import_statement.split('.')[0]
                        if package and package not in imported_packages:
                            imported_packages.add(package)
    return imported_packages


installed_packages = get_installed_packages()

# Get the list of imported packages in the current directory
imported_packages = parse_imports('.')


required_packages = {pkg: installed_packages[pkg] for pkg in imported_packages if pkg in installed_packages}

# Write the required packages to requirements.txt
with open('requirements.txt', 'w') as f:
    for pkg in required_packages.values():
        f.write(pkg + '\n')

print("requirements.txt has been generated.")
