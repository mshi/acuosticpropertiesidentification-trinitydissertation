import numpy as np  
import pympi
import pandas as pd


import glob
import random
import os
import pandas as pd
import os
import PartipantNode
from lxml import etree


class GlobalTimeLine:
    def __init__(self, eaf_address):
        self.eaf_address = eaf_address
        self.globalList_currentSession = []
        self.timeDF = None

    # This is for the total Timeline not indivual
    def construct(self):

        Conversational_TYPE_Silence = "Silence"
        Conversational_LaughterSection = "Laughter"
        Conversational_NonLaughterSection = "Non Laughter"

        Conversational_TYPE_Mirthful = "Mirthful"
        Conversational_TYPE_Discourse = "Discourse"
        Conversational_TYPE_Vocalization = "Vocalization"

        myeafaddress = self.eaf_address

        tree = etree.parse(myeafaddress)
        root = tree.getroot()

        time_slot_ids = []
        time_values = []
        time_order = root.find(".//TIME_ORDER")
        if time_order is not None:
            for time_slot in time_order.findall(".//TIME_SLOT"):
                time_slot_id = time_slot.get("TIME_SLOT_ID")
                time_value = time_slot.get("TIME_VALUE")
                time_slot_ids.append(time_slot_id)
                time_values.append(time_value)

        # Create a Pandas DataFrame
        dic_forTimeSlot = {"TIME_SLOT_ID": time_slot_ids, "TIME_VALUE": time_values}
        timeSlot_df = pd.DataFrame(dic_forTimeSlot)

        lastTimeStamp = int(timeSlot_df.iloc[-1, 1])

        # Loading the eaf file

        elan_file = pympi.Elan.Eaf(myeafaddress)

        start_times = []
        end_times = []
        tier_names = []
        annotations = []

        for tier in elan_file.get_tier_names():
            annotations_in_tier = elan_file.get_annotation_data_for_tier(tier)

            for annotation in annotations_in_tier:
                start_times.append(annotation[0])
                end_times.append(annotation[1])
                tier_names.append(tier)
                annotations.append(annotation[2])

        data = {
            "Tier": tier_names,
            "Start Time": start_times,
            "End Time": end_times,
            "Annotation": annotations,
        }

        myeafdf = pd.DataFrame(data)

        # Finishing loading

        # Start timelien algorithm

        laughterSection = myeafdf[(myeafdf["Tier"] == "laughter_section")]
        nonlaughter_section = myeafdf[(myeafdf["Tier"] == "nonlaughter_section")]

        start_laughterSection = laughterSection[["Start Time"]]
        start_laughterSection = start_laughterSection["Start Time"].tolist()

        end_laughterSection = laughterSection[["End Time"]]
        end_laughterSection = end_laughterSection["End Time"].tolist()

        start_non_laughterSection = nonlaughter_section[["Start Time"]]
        start_non_laughterSection = start_non_laughterSection["Start Time"].tolist()

        end_non_laughterSection = nonlaughter_section[["End Time"]]
        end_non_laughterSection = end_non_laughterSection["End Time"].tolist()

        # Four pointer and timeLine pointer

        pointer_To_start_laughterSection = 0
        pointer_To_end_laughterSection = 0
        pointer_To_start_non_laughterSection = 0
        pointer_To_end_non_laughterSection = 0
        current_timeStampPointer = 0

        # We don't know the paricipat ID
        tierColumn = myeafdf["Tier"]
        tierColumnList = tierColumn.tolist()
        element_count = {}
        for element in tierColumnList:
            if element in element_count:
                element_count[element] += 1
            else:
                element_count[element] = 1
        unique_tierColumn_list = list(set(tierColumnList))
        participant__ID_array = [
            element for element in unique_tierColumn_list if element.startswith("P")
        ]
        moderator_ID_array = [
            element for element in unique_tierColumn_list if element.startswith("M")
        ]

        participantList = []
        moderatorElement = moderator_ID_array[0]
        participantOne = participant__ID_array[1]
        participantTwo = participant__ID_array[0]
        participantList.append(moderatorElement)
        participantList.append(participantOne)
        participantList.append(participantTwo)
        SessionID = moderatorElement[-3:]

        #  Timeline reconstruct Algorithm
        # Assume we don't need the time in the EAF fie
        # For rigours constructing the the whole time line

        # globalTimeline = [] for store
        #  StartTime | EndTime | Conversational type - Laughter or non laughter
        # globalList_currentSession = []
        self.globalList_currentSession.append(
            ["Start Time-ms", "End time-ms", "Conversational Type"]
        )
        # Marco varibale

        #  This variable set two state inital start and other state
        controlLogic = 0
        while current_timeStampPointer <= lastTimeStamp:

            # Using control logic to turn state
            #  Debug here
            # 619439
            # 620718
            # 622300
            if current_timeStampPointer == 622300:
                break
            if current_timeStampPointer == 619439:
                sss = 1
            if controlLogic == 0:
                # I need to dicuss two cases the start time is 0
                # Or not to determine the start time of laughter or non-laughter non
                # I need to know the min number of start of laughter or non-laughter
                minLaughterStart = start_laughterSection[0]
                minnonLaughterStart = start_non_laughterSection[0]
                minStartTime = min(minLaughterStart, minnonLaughterStart)
                # Not silience
                # Siliene [0,...minStartTime]
                if minStartTime != 0:
                    silience_start = 0
                    silience_end = minStartTime
                    self.globalList_currentSession.append(
                        [silience_start, silience_end, Conversational_TYPE_Silence]
                    )

                    current_timeStampPointer = silience_end

                else:
                    # In this timeline the start time is zero
                    # At least non-laughter or laughter start time equal to zero

                    minLaughterStart = start_laughterSection[0]
                    minnonLaughterStart = start_non_laughterSection[0]

                    minStartTime = min(minLaughterStart, minnonLaughterStart)
                    current_conversationTag = ""
                    if minStartTime == minLaughterStart:
                        current_conversationTag = Conversational_LaughterSection
                    elif minStartTime == minnonLaughterStart:
                        current_conversationTag = Conversational_NonLaughterSection
                    # Further judgement
                    # minStartTime = 0 at the moment
                    if current_conversationTag == Conversational_LaughterSection:
                        # load the start time Pointer++
                        startTime = start_laughterSection[
                            pointer_To_start_laughterSection
                        ]
                        pointer_To_start_laughterSection += 1

                        # Loading content of end time pointer to end
                        endTime = end_laughterSection[pointer_To_end_laughterSection]
                        # Set the currentTimeStamp to the end point of this section
                        pointer_To_end_laughterSection += 1
                        current_timeStampPointer = endTime

                        # Find the startime end time in laughter array
                        # laughterSection
                        specificLaughterRow = laughterSection[
                            (laughterSection["Start Time"] == startTime)
                            & (laughterSection["End Time"] == endTime)
                        ]

                        currentLaughter = specificLaughterRow["Annotation"].tolist()

                        currentLaughter = currentLaughter[0]

                        self.globalList_currentSession.append(
                            [startTime, endTime, currentLaughter]
                        )

                    elif current_conversationTag == Conversational_NonLaughterSection:
                        # load the start time Pointer++
                        startTime = start_non_laughterSection[
                            pointer_To_start_non_laughterSection
                        ]
                        pointer_To_start_non_laughterSection += 1

                        # Loading content of end time pointer to end
                        endTime = end_non_laughterSection[
                            pointer_To_end_non_laughterSection
                        ]
                        # Set the currentTimeStamp to the end point of this section
                        pointer_To_end_non_laughterSection += 1
                        current_timeStampPointer = endTime

                        self.globalList_currentSession.append(
                            [startTime, endTime, Conversational_NonLaughterSection]
                        )

                # Turn to another state
                controlLogic = 1

            if controlLogic != 0:
                infiniteNumber = 2222222222222222

                # Determine whether last time stamp equal to the start time of lagher or nonlaugher
                
                lengthOfStartLaughter = len(start_laughterSection)
                lengthOfStartNonLaughter = len(start_non_laughterSection)
                startTimeLS = 0

                tagForStartLaugherVaild = True
                tagForStartNonLaugherVaild = True
                if pointer_To_start_laughterSection < lengthOfStartLaughter:
                    startTimeLS = start_laughterSection[
                        pointer_To_start_laughterSection
                    ]
                else:
                    startTimeLS = infiniteNumber
                    tagForStartLaugherVaild = False
                # startTimeLS = start_laughterSection[pointer_To_start_laughterSection]
                #   if startTimeLS>=lengthOfStartLaughter:
                #       startTimeLS = infiniteNumber

                # current_timeStampPointer ==start_non_laughterSection[pointer_To_start_non_laughterSection]
                startTimeNonL = 0

                if pointer_To_start_non_laughterSection < lengthOfStartNonLaughter:
                    startTimeNonL = start_non_laughterSection[
                        pointer_To_start_non_laughterSection
                    ]
                else:
                    startTimeNonL = 3333333333333333
                    tagForStartNonLaugherVaild = False

                #    if startTimeNonL >= lengthOfStartNonLaughter:
                #        startTimeNonL = infiniteNumber
                #  if startTimeLS >= lengthOfStartLaughter:
                #     startTimeLS = None
                #   if startTimeNonL >= lengthOfStartNonLaughter:
                #      startTimeNonL = None
                if (
                    current_timeStampPointer == startTimeLS
                    or current_timeStampPointer == startTimeNonL
                ):

                    currentStartTime = 0

                    currentEndTime = 0
                    #  I need to secury a
                    if (
                        tagForStartLaugherVaild == True
                        or tagForStartNonLaugherVaild == True
                    ):

                        if tagForStartLaugherVaild == True:
                            if (
                                current_timeStampPointer
                                == start_laughterSection[
                                    pointer_To_start_laughterSection
                                ]
                            ):
                                currentStartTime = start_laughterSection[
                                    pointer_To_start_laughterSection
                                ]
                                pointer_To_start_laughterSection += 1

                                currentEndTime = end_laughterSection[
                                    pointer_To_end_laughterSection
                                ]
                                pointer_To_end_laughterSection += 1

                                current_timeStampPointer = currentEndTime

                                specificLaughterRow = laughterSection[
                                    (laughterSection["Start Time"] == currentStartTime)
                                    & (laughterSection["End Time"] == currentEndTime)
                                ]

                                currentLaughter = specificLaughterRow[
                                    "Annotation"
                                ].tolist()

                                currentLaughter = currentLaughter[0]

                                self.globalList_currentSession.append(
                                    [currentStartTime, currentEndTime, currentLaughter]
                                )
                        if tagForStartNonLaugherVaild == True:

                            if (
                                current_timeStampPointer
                                == start_non_laughterSection[
                                    pointer_To_start_non_laughterSection
                                ]
                            ):
                                currentStartTime = start_non_laughterSection[
                                    pointer_To_start_non_laughterSection
                                ]
                                pointer_To_start_non_laughterSection += 1

                                currentEndTime = end_non_laughterSection[
                                    pointer_To_end_non_laughterSection
                                ]
                                pointer_To_end_non_laughterSection += 1

                                current_timeStampPointer = currentEndTime

                                self.globalList_currentSession.append(
                                    [
                                        currentStartTime,
                                        currentEndTime,
                                        Conversational_NonLaughterSection,
                                    ]
                                )

                # If not in
                # Since the value in the start time is annoted in asecindg order
                # I do not need to sort again
                else:
                    # This time stamp is from preivous stamp
                    tempSlienceStart = current_timeStampPointer

                    lengthOfStartLaughter = len(start_laughterSection)
                    lengthOfStartNonLaughter = len(start_non_laughterSection)
                    startTimeLS = 0
                    startTimeNonLS = 0
                    currentStartTimeInLaugherArray = 0
                    currentStartTimeInNonLaughterArray = 0
                    if pointer_To_start_laughterSection < lengthOfStartLaughter:
                        currentStartTimeInLaugherArray = start_laughterSection[
                            pointer_To_start_laughterSection
                        ]
                    else:
                        currentStartTimeInLaugherArray = None

                    if pointer_To_start_non_laughterSection < lengthOfStartNonLaughter:
                        currentStartTimeInNonLaughterArray = start_non_laughterSection[
                            pointer_To_start_non_laughterSection
                        ]

                    else:
                        currentStartTimeInNonLaughterArray = None

                    # Checing existing not end end
                    if (
                        currentStartTimeInLaugherArray != None
                        or currentStartTimeInNonLaughterArray != None
                    ):
                        # I need to further judge again
                        # Checking thse threr cases separately
                        if (
                            currentStartTimeInLaugherArray != None
                            and currentStartTimeInNonLaughterArray != None
                        ):
                            minSectionStartTimeSelection = min(
                                currentStartTimeInLaugherArray,
                                currentStartTimeInNonLaughterArray,
                            )
                            tempSlienceEnd = minSectionStartTimeSelection
                            self.globalList_currentSession.append(
                                [
                                    tempSlienceStart,
                                    tempSlienceEnd,
                                    Conversational_TYPE_Silence,
                                ]
                            )

                        elif currentStartTimeInLaugherArray == None:
                            # This meain we check frm non laugher
                            tempSlienceEnd = currentStartTimeInNonLaughterArray
                            self.globalList_currentSession.append(
                                [
                                    tempSlienceStart,
                                    tempSlienceEnd,
                                    Conversational_TYPE_Silence,
                                ]
                            )

                        elif currentStartTimeInNonLaughterArray == None:
                            # This meain we check frm  laugher
                            tempSlienceEnd = currentStartTimeInNonLaughterArray
                            self.globalList_currentSession.append(
                                [
                                    tempSlienceStart,
                                    tempSlienceEnd,
                                    Conversational_TYPE_Silence,
                                ]
                            )
                    # Then check there isn't stattim either array which mean end of itme

                    else:
                        tempSlienceEnd = lastTimeStamp
                        current_timeStampPointer = current_timeStampPointer + 100
                        self.globalList_currentSession.append(
                            [
                                tempSlienceStart,
                                tempSlienceEnd,
                                Conversational_TYPE_Silence,
                            ]
                        )

        IndexColumn = ["Start Time-ms", "End time-ms", "Conversational Type"]
        del self.globalList_currentSession[0]
        mydef_global_pd = pd.DataFrame(self.globalList_currentSession, columns=IndexColumn)
        last_row = mydef_global_pd.iloc[-1, 1]

        mask = mydef_global_pd["End time-ms"] == last_row
        series_list = mask.tolist()

        # Using list comprehension to get indices where the value is True
        true_indices_for_duplicated = [index for index, value in enumerate(series_list) if value]

        theminum_row = true_indices_for_duplicated[0]
        get_rowminue = mydef_global_pd.iloc[theminum_row]
        mydef_new = mydef_global_pd.drop(true_indices_for_duplicated)

        mydef_new.loc[len(mydef_new)] = get_rowminue

        s = 1

        self.timeDF = mydef_new

    def get_time_df(self):
        return self.timeDF



"""

first get the value in the second column in the last row. let deonote this value as A

Then iterate all the pandas, find all the rows whoose second value equal to value A.We only maintain the least value in the first value whoose second value is value A
"""

#  A = self.globalList_currentSession.iloc[-1, 1]

#   df_no_duplicates = temp_df[~temp_df.duplicated(keep=False)]
# self.timeDFtemp_df = df_filtered
