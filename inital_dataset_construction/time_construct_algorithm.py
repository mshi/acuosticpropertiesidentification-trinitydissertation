import numpy as np  
import pympi
import pandas as pd

import glob
import random
import os
import pandas as pd
import os
import PartipantNode
from lxml import etree


class TimeConstruct:
    def __init__(self, eaf_address):
        self.eaf_address = eaf_address
        self.globalList_currentSession = []
        self.timeDF = None
        self.session_id = ""

    def pands_to_list_of_list(self, input_df):
        final_list = []

        for index, row in input_df.iterrows():
            start_time = row[1]
            end_time = row[2]
            annotation = row[3]
            temp_list = [start_time, end_time, annotation]
            final_list.append(temp_list)

        return final_list

    def participant_list(self):

        # Loading the eaf file
        myeafaddress = self.eaf_address

        elan_file = pympi.Elan.Eaf(myeafaddress)

        start_times = []
        end_times = []
        tier_names = []
        annotations = []

        for tier in elan_file.get_tier_names():
            annotations_in_tier = elan_file.get_annotation_data_for_tier(tier)

            for annotation in annotations_in_tier:
                start_times.append(annotation[0])
                end_times.append(annotation[1])
                tier_names.append(tier)
                annotations.append(annotation[2])

        data = {
            "Tier": tier_names,
            "Start Time": start_times,
            "End Time": end_times,
            "Annotation": annotations,
        }

        myeafdf = pd.DataFrame(data)

        self.pandas_whole = myeafdf

        # We don't know the paricipat ID
        tierColumn = myeafdf["Tier"]
        tierColumnList = tierColumn.tolist()
        element_count = {}
        for element in tierColumnList:
            if element in element_count:
                element_count[element] += 1
            else:
                element_count[element] = 1
        unique_tierColumn_list = list(set(tierColumnList))
        participant__ID_array = [
            element for element in unique_tierColumn_list if element.startswith("P")
        ]
        moderator_ID_array = [
            element for element in unique_tierColumn_list if element.startswith("M")
        ]

        participantList = []
        moderatorElement = moderator_ID_array[0]
        participantOne = participant__ID_array[1]
        participantTwo = participant__ID_array[0]
        participantList.append(moderatorElement)
        participantList.append(participantOne)
        participantList.append(participantTwo)

        SessionID = moderatorElement[-3:]
        self.session_id = SessionID

        return participantList

    """
    @ Transform the text input
    """

    def text_transfromation(self, input_string):
        pass

    def construct(self):

        # Maro varaible

        Conversational_TYPE_Silence = "Silence"

        Conversational_LaughterSection = "Laughter"
        Conversational_NonLaughterSection = "Non Laughter"

        laughter_pattern = "[laugh]"
        # Parse XML file for some pattern
        myeafaddress = self.eaf_address
        #  To get the last time stamp
        tree = etree.parse(myeafaddress)
        root = tree.getroot()

        time_slot_ids = []
        time_values = []
        time_order = root.find(".//TIME_ORDER")
        if time_order is not None:
            for time_slot in time_order.findall(".//TIME_SLOT"):
                time_slot_id = time_slot.get("TIME_SLOT_ID")
                time_value = time_slot.get("TIME_VALUE")
                time_slot_ids.append(time_slot_id)
                time_values.append(time_value)

        # Create a Pandas DataFrame
        dic_forTimeSlot = {"TIME_SLOT_ID": time_slot_ids, "TIME_VALUE": time_values}
        timeSlot_df = pd.DataFrame(dic_forTimeSlot)

        lastTimeStamp = int(timeSlot_df.iloc[-1, 1])

        # Loading the eaf file

        elan_file = pympi.Elan.Eaf(myeafaddress)

        start_times = []
        end_times = []
        tier_names = []
        annotations = []

        for tier in elan_file.get_tier_names():
            annotations_in_tier = elan_file.get_annotation_data_for_tier(tier)

            for annotation in annotations_in_tier:
                start_times.append(annotation[0])
                end_times.append(annotation[1])
                tier_names.append(tier)
                annotations.append(annotation[2])

        data = {
            "Tier": tier_names,
            "Start Time": start_times,
            "End Time": end_times,
            "Annotation": annotations,
        }

        myeafdf = pd.DataFrame(data)

        # Load the particiapnt

        # to get the tier name such as "M001_S2"
        current_participant_list = self.participant_list()
        current_moderator_tier = current_participant_list[0]
        current_participant_tier_one = current_participant_list[1]
        current_participant_tier_two = current_participant_list[2]
        # Construct three pd
        current_moderator_tier_pd = myeafdf[(myeafdf["Tier"] == current_moderator_tier)]
        current_participant_tier_one_pd = myeafdf[
            (myeafdf["Tier"] == current_participant_tier_one)
        ]
        current_participant_tier_two_pd = myeafdf[
            (myeafdf["Tier"] == current_participant_tier_two)
        ]
        # Laughter section is for replace the absutcut [laugh] to the specific laugh
        # Such as Mirthful and Discourse.

        laughter_Section = myeafdf[(myeafdf["Tier"] == "laughter_section")]
        #  Two data structure needed here
        #  The first one is
        """
        start time,end time,annotation
        [0,1375,"laugh"]
        """
        current_moderator_tier_listOfList = self.pands_to_list_of_list(
            current_moderator_tier_pd
        )
        current_participant_tier_one_listOfList = self.pands_to_list_of_list(
            current_participant_tier_one_pd
        )

        current_participant_tier_two_listOfList = self.pands_to_list_of_list(
            current_participant_tier_two_pd
        )

        # Another data structure is the
        # each paricipant should split the start time and end time for each
        ###                        Transform to series
        start_time_string = "Start Time"
        end_time_string = "End Time"

        start_moderator_series = current_moderator_tier_pd[["Start Time"]]
        end_moderator_series = current_moderator_tier_pd[["End Time"]]

        start_participant_one_series = current_participant_tier_one_pd[["Start Time"]]
        end_participant_one_series = current_participant_tier_one_pd[["End Time"]]

        start_participant_two_series = current_participant_tier_two_pd[["Start Time"]]
        end_participant_two_series = current_participant_tier_two_pd[["End Time"]]

        # Transform the pandas series to list

        start_moderator_array = start_moderator_series[start_time_string].tolist()
        end_moderator_array = end_moderator_series[end_time_string].tolist()

        start_participant_one_array = start_participant_one_series[
            start_time_string
        ].tolist()
        end_participant_one_array = end_participant_one_series[end_time_string].tolist()

        start_participant_two_array = start_participant_two_series[
            start_time_string
        ].tolist()
        end_participant_two_array = end_participant_two_series[end_time_string].tolist()

        #  The array is to construct time
        # The list of list is to locate the specific type

        # six pointer and time pointer

        pointer_To_start_moderator = 0
        pointer_To_end_lmoderator = 0
        pointer_To_start_participant_one = 0
        pointer_To_end_participant_one = 0

        pointer_To_start_participant_two = 0
        pointer_To_end_participant_two = 0

        current_timeStampPointer = 0

        self.globalList_currentSession.append(
            ["Start Time-ms", "End time-ms", "Conversational Type"]
        )

        # Marco varibale

        #  This variable set two state inital start and other state
        controlLogic = 0
        while current_timeStampPointer <= lastTimeStamp:
            if controlLogic == 0:
                # I need to dicuss two cases the start time is 0
                #
                # I need to know the min number of start of laughter or non-laughter
                min_moderator_start_time_inital = start_moderator_array[0]
                min_participant_one_start_time_inital = start_participant_one_array[0]
                min_participant_two_start_time_inital = start_participant_two_array[0]

                minStartTime = min(
                    min_moderator_start_time_inital,
                    min_participant_one_start_time_inital,
                    min_participant_two_start_time_inital,
                )

                # Not silience
                # Siliene [0,...minStartTime]
                if minStartTime != 0:

                    silience_start = 0
                    silience_end = minStartTime
                    # Here we don't need to record
                    self.globalList_currentSession.append(
                        [silience_start, silience_end, Conversational_TYPE_Silence]
                    )

                    current_timeStampPointer = silience_end

                else:
                    # In this timeline the start time is zero
                    # At least non-laughter or laughter start time equal to zero

                    min_moderator_start_time_inital = start_moderator_array[0]
                    min_participant_one_start_time_inital = start_participant_one_array[
                        0
                    ]
                    min_participant_two_start_time_inital = start_participant_two_array[
                        0
                    ]
                    # min_start time
                    minStartTime = min(
                        min_moderator_start_time_inital,
                        min_participant_one_start_time_inital,
                        min_participant_two_start_time_inital,
                    )

                    # Add
                    if minStartTime == min_moderator_start_time_inital:
                        start_time_m = start_moderator_array[0]
                        end_time_m = end_moderator_array[0]
                        conversation_type = ''
                        for inner_list in current_moderator_tier_listOfList:
                            list_start_time = inner_list[0]
                            list_end_time = inner_list[1]
                            if start_time_m == list_start_time and end_time_m == list_end_time:
                                # Add the conversation type judge
                                annotations_in_list = inner_list[2]
                                if annotations_in_list == laughter_pattern:
                                    conversation_type = Conversational_LaughterSection
                                else:
                                    conversation_type = Conversational_NonLaughterSection
                        # consider the rest of four pointers here and four array
                        


                        # Store  and pointer moving for current speaker pointer
                        current_temp_list = [start_time_m, end_time_m, conversation_type]
                        # increase the pointer for this speaker both for time time and end time
                        pointer_To_start_moderator += 1

                        current_timeStampPointer = pointer_To_end_lmoderator

                        pointer_To_end_lmoderator += 1

                        self.globalList_currentSession.append(current_temp_list)

                    elif minStartTime == min_participant_one_start_time_inital:
                        start_time_p1 = start_participant_one_array[0]
                        end_time_p1 = end_participant_one_array[0]
                        conversation_type = ''
                        for inner_list in current_participant_tier_one_listOfList:
                            list_start_time = inner_list[0]
                            list_end_time = inner_list[1]
                            if start_time_p1 == list_start_time and end_time_p1 == list_end_time:
                                # Add the conversation type judge
                                annotations_in_list = inner_list[2]
                                if annotations_in_list == laughter_pattern:
                                    conversation_type = Conversational_LaughterSection
                                else:
                                    conversation_type = Conversational_NonLaughterSection
                        current_temp_list = [start_time_p1, end_time_p1, conversation_type]
                        # Increase the pointer
                        pointer_To_start_participant_one += 1

                        current_timeStampPointer = pointer_To_end_participant_one
                        pointer_To_end_participant_one += 1
                        self.globalList_currentSession.append(current_temp_list)


                    elif minStartTime == min_participant_two_start_time_inital:

                        start_time_p2 = start_participant_two_array[0]
                        end_time_p2 = end_participant_two_array[0]
                        conversation_type = ''
                        for inner_list in current_participant_tier_two_listOfList:
                            list_start_time = inner_list[0]
                            list_end_time = inner_list[1]
                            if start_time_p2 == list_start_time and end_time_p2 == list_end_time:
                                # Add the conversation type judge
                                annotations_in_list = inner_list[2]
                                if annotations_in_list == laughter_pattern:
                                    conversation_type = Conversational_LaughterSection
                                else:
                                    conversation_type = Conversational_NonLaughterSection
                        current_temp_list = [start_time_p2, end_time_p2, conversation_type]
                        # Increase the pointer
                        pointer_To_start_participant_two += 1
                        current_timeStampPointer = pointer_To_end_participant_two
                        pointer_To_end_participant_two += 1
                        self.globalList_currentSession.append(current_temp_list)
                # Change another stage
                controlLogic :int= 1
            # We need to set the timestamp pointer to the endtime of current speaker
            if controlLogic != 0:
                current_time_moderator_start_time = start_moderator_array[pointer_To_start_moderator]
                current_time_participant_one_start_time = start_participant_one_array[pointer_To_start_participant_one]
                current_time_participant_two_start_time = start_participant_two_array[start_participant_two_array]
                # Determine whether last time stamp equal to the start time of lagher or nonlaugher

                length_of_moderator_start_time = len(current_time_moderator_start_time)
                length_of_participant_one_start_time = len(current_time_participant_one_start_time)

                length_of_participant_two_start_time = len(current_time_participant_two_start_time)

                current_startTimeIn_moderatorArray = 0
                current_startTimeIn_Participant_One_Array = 0
                current_startTimeIn_Participant_Two_Array = 0

                # Security Check here
                    #Check moderator
#--------------------------------------------------------------------------------------------

                infinite_postive_number = np.inf
                tag_ForStart_moderator_Vaild = True
                tag_ForStart_participant_One_Vaild = True
                tag_ForStart_participant_Two_Vaild = True
                if pointer_To_start_moderator < length_of_moderator_start_time:
                    current_startTimeIn_moderatorArray = start_moderator_array[
                        pointer_To_start_moderator
                    ]
                else:
                    current_startTimeIn_moderatorArray = infinite_postive_number
                    tag_ForStart_moderator_Vaild = False

                if pointer_To_start_participant_one <length_of_participant_one_start_time:
                    current_startTimeIn_Participant_One_Array = start_participant_one_array[
                        pointer_To_start_participant_one
                    ]
                else:
                    current_startTimeIn_Participant_One_Array = infinite_postive_number
                    tag_ForStart_participant_One_Vaild = False

                if pointer_To_start_participant_two <length_of_participant_two_start_time:
                    current_startTimeIn_Participant_Two_Array = start_participant_two_array[
                        pointer_To_start_participant_two
                    ]
                else:
                    current_startTimeIn_Participant_Two_Array = infinite_postive_number
                    tag_ForStart_participant_Two_Vaild = False

# --------------------------------------------------------------------------------------------

                # current step is check not the end
                # Hence I need to check the three value here
                #       current_startTimeIn_moderatorArray = 0
                #current_startTimeIn_Participant_One_Array = 0
                #current_startTimeIn_Participant_Two_Array = 0

                if (
                        current_timeStampPointer == current_startTimeIn_moderatorArray
                        or current_timeStampPointer == current_startTimeIn_Participant_One_Array
                        or  current_timeStampPointer == current_startTimeIn_Participant_Two_Array
                ):
                    '''
                    tag_ForStart_moderator_Vaild = True
                    tag_ForStart_participant_One_Vaild = True
                    tag_ForStart_participant_Two_Vaild = True
                    '''
                    if (
                            tag_ForStart_moderator_Vaild == True
                            or tag_ForStart_participant_One_Vaild == True
                            or tag_ForStart_participant_Two_Vaild == True
                    ):
                        # Since this is three participant
                        pass




                if( current_startTimeIn_moderatorArray != infinite_postive_number
                    or current_startTimeIn_Participant_One_Array != infinite_postive_number
                    or current_startTimeIn_Participant_Two_Array != infinite_postive_number):

                    min_start_time_in_this_moment = min(current_startTimeIn_moderatorArray,
                                                        current_startTimeIn_Participant_One_Array,
                                                        current_startTimeIn_Participant_Two_Array)
                    start_time_this_moment = min_start_time_in_this_moment

                    s =2
                else:
                    pass