

# Since we need to insert content arbitary and henece We select linklist

# Node for start time and end time and conversation tyep
class TimeStampNode:
    def __init__(self,start_time, end_time,conversation_type):
        self.start_time = start_time
        self.end_time = end_time
        self.conversation_type = conversation_type
        self.next = None

class Interlocutor_linklist:
    def __init__(self):
        self.head = None

    def add(self, start_time, end_time, conversation_type):
        new_node = TimeStampNode(start_time, end_time, conversation_type)
        if not self.head or self.head.start_time > start_time:
            new_node.next = self.head
            self.head = new_node
        else:
            current = self.head
            while current.next and current.next.start_time < start_time:
                current = current.next

            new_node.next = current.next
            current.next = new_node
        self.fill_silience_gap()

# Fill the gap Automatically
    def fill_silience_gap(self):
        current = self.head
        silience_str ='Silience'
        while current and current.next:
            if current.end_time != current.next.start_time:
                gap_node = TimeStampNode(current.end_time, current.next.start_time, silience_str)
                gap_node.next = current.next
                current.next = gap_node
            current = current.next

    def print_list(self):
        cur_node = self.head
        while cur_node:
            print(f"Start Time: {cur_node.start_time}, End Time: {cur_node.end_time}, Conversation Type: {cur_node.conversation_type}")
            cur_node = cur_node.next
# Return list of list
    # particpant id,list
    def transform_linklist_to_listOflist(self):
        transform_list = []
        cur_node = self.head
        while cur_node:
            star_time = cur_node.start_time
            end_time = cur_node.end_time
            duration = end_time-star_time
            conversation_type = cur_node.conversation_type
            temp_list = [star_time,end_time,duration,conversation_type]
            transform_list.append(temp_list)

            cur_node = cur_node.next

        return transform_list

    # Since in the Tier P_007  only contain laughter
    # but in the laughter section tier contain the specif type
    # That is why we need to modify the conten of ndoe
    def from_abstruct_laughter_to_specific(self,startTime,endTime,ChangedAnnotion):
        cur_node = self.head
        while cur_node:
            currentStartEndTime = cur_node.start_time
            currentEndTime  = cur_node.end_time
            if(currentStartEndTime == startTime and currentEndTime == endTime):
                #Extract from array
                cur_node.conversation_type =ChangedAnnotion
            cur_node = cur_node.next
    # Find the last time node the node for iteration to insert a gap
    # Gap start time is the end time of the last node and
    # end time is last timestamp
    def identify_last_node_end_time(self):
        current = self.head
        while current and current.next:
            current = current.next

        end_last_time_node = current.end_time
        return end_last_time_node


