import os

import pandas as pd
import pympi
from verification_natural_laughter import  verification_to_natural_laughter
from vocalisation_classification import Classfication_funtion
from PartipantNode import TimeStampNode, Interlocutor_linklist
from lxml import etree
from verification_social_laughter import verification_to_socicalLaughter
from stack import Stack
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import word_tokenize
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize

# For the mono filer ,the small number are left L
# tHE LARGE NUMBER ARE RIGHT
class inital_datasetConstruction:
    def __init__(self, eaf_address):
        self.eaf_address: str = eaf_address
        self.terminated: int = 0
        self.session_id: str = ''
        self.time_line_df = ''
        self.moderator_list = []
        self.participant_one_list: list = []


        self.participiant_two_list: list = []
        # we don not need to parse again the eaf
        self.pandas_whole = None
        self.last_time_stamp: int = 0

    def change_abstruct_laughter_to_specific(self, current_laughter_tuple: tuple, laughter_section_list) -> str:

        start_time_participant = current_laughter_tuple[0]
        end_time_participant = current_laughter_tuple[1]
        for inner_list in laughter_section_list:
            start_time_inner_list = inner_list[0]
            end_time_inner_list = inner_list[1]

            if start_time_inner_list == 524727:
                sss = 2;

            firstcondition = (start_time_participant>=start_time_inner_list)
            secondCondtion = (end_time_participant<=end_time_inner_list)

            combination_conditionOne = (start_time_participant == start_time_inner_list and end_time_participant == end_time_inner_list )

            combintion_conditionTwo =(firstcondition and secondCondtion)

            combintionCondition_three = (start_time_participant<=start_time_inner_list and end_time_participant >=end_time_inner_list)
            if combination_conditionOne or combintion_conditionTwo or combintionCondition_three:
                specific_laughter = inner_list[2]
                return specific_laughter
        return "null"

    def preprocessing_str_stemming(self,input_str:str)->str:
        current_str:str = input_str


        # Create a Snowball stemmer
        stemmer = SnowballStemmer("english")

        words = word_tokenize(current_str)

        # Stem each word in the string
        stemmed_words = [stemmer.stem(word) for word in words]
        stemmed_string = " ".join(stemmed_words)
        return  stemmed_string.strip()
    def vocalisation_classification(self,input_str: str) -> str:
        lowercase_input: str = input_str.lower().strip()

        left_bracket_marco: str = '['
        right_bracket_marco: str = "]"
        #
        return_str: str = ''
        store_list: list[str] = []
        vocalisation_button: bool = False
        bracket_stack: Stack = Stack()
        # we need to determine the index = length_of_string -1
        length_of_string: int = len(input_str)
        punctuations_symbols:list[str] = ['!', '?' , '.' , ';' , ',']
        for index_string, current_character in enumerate(lowercase_input):
            current_size_stack: int = bracket_stack.__len__()

            # make sure current string is not [ or ]
            # vocalisation_button open when the current is not button
            # vocalisation_button when enounter [ close

            # current character is normal symbol and stack is empty V is within the [
            if current_size_stack == 0:
                if current_character != left_bracket_marco and current_character != right_bracket_marco:
                    vocalisation_button = True
                    # last one
                    if index_string == (length_of_string - 1):
                        if current_character in punctuations_symbols:
                            # store this symbol
                            # return_str+=current_character
                            if vocalisation_button == True:
                                store_list.append('V')
                                store_list.append(current_character)


                        else:
                            ## normal charavtr
                            if vocalisation_button == True:
                                store_list.append('V')
                                # return_str+='V'
                            else:
                                pass
                    else:
                        # do nothing just wait
                        pass

                elif current_character == left_bracket_marco:
                    # add condition here
                    if vocalisation_button == True:
                        store_list.append('V')

                    bracket_stack.push(current_character)
                    vocalisation_button = True
                # currrent_ stack size is not zero
            else:
                # V within [
                if current_character != right_bracket_marco:
                    # bracket_stack.pop()
                    vocalisation_button = True


                # other case
                else:
                    # append string here
                    str_to_add: str = '[V]'
                    bracket_stack.pop()
                    store_list.append(str_to_add)

                    vocalisation_button = False
            # sdsds?

        size_of_list_store = len(store_list)

        if size_of_list_store == 1:
            return_str = store_list[0]
        else:
            # V sequndece
            for index_list, curr_str in enumerate(store_list):

                if index_list != (size_of_list_store - 1):

                    if index_list == 0:
                        return_str += curr_str

                    else:
                        return_str += ' '
                        return_str += curr_str

                else:
                    # judge symbol
                    if curr_str in punctuations_symbols:

                        return_str += curr_str
                    else:
                        return_str += ' '
                        return_str += curr_str

        return return_str

    def pands_to_list_of_list(self, input_df) -> list:
        final_list = []

        for index, row in input_df.iterrows():
            start_time = row.iloc[1]
            end_time = row.iloc[2]
            annotation = row.iloc[3]
            temp_list = [start_time, end_time, annotation]
            final_list.append(temp_list)

        return final_list

    def participant_list(self) -> list:

        # Loading the eaf file
        myeafaddress = self.eaf_address

        elan_file = pympi.Elan.Eaf(myeafaddress)

        start_times = []
        end_times = []
        tier_names = []
        annotations = []

        for tier in elan_file.get_tier_names():
            annotations_in_tier = elan_file.get_annotation_data_for_tier(tier)

            for annotation in annotations_in_tier:
                start_times.append(annotation[0])
                end_times.append(annotation[1])
                tier_names.append(tier)
                annotations.append(annotation[2])

        data = {
            "Tier": tier_names,
            "Start Time": start_times,
            "End Time": end_times,
            "Annotation": annotations,
        }

        myeafdf = pd.DataFrame(data)

        self.pandas_whole = myeafdf

        # We don't know the paricipat ID
        tierColumn = myeafdf['Tier']
        tierColumnList = tierColumn.tolist()
        element_count = {}
        for element in tierColumnList:
            if element in element_count:
                element_count[element] += 1
            else:
                element_count[element] = 1
        unique_tierColumn_list = list(set(tierColumnList))
        participant__ID_array = [element for element in unique_tierColumn_list if element.startswith('P')]
        moderator_ID_array = [element for element in unique_tierColumn_list if element.startswith('M')]

        participantList = []
        moderatorElement = moderator_ID_array[0]
        participantOne = participant__ID_array[1]
        participantTwo = participant__ID_array[0]
        participantList.append(moderatorElement)
        participantList.append(participantOne)
        participantList.append(participantTwo)

        SessionID = moderatorElement[-3:]
        self.session_id = SessionID

        return participantList

    def get_lastTime_stamp(self) -> int:

        tree = etree.parse(self.eaf_address)
        root = tree.getroot()

        time_slot_ids = []
        time_values = []
        time_order = root.find(".//TIME_ORDER")
        if time_order is not None:
            for time_slot in time_order.findall(".//TIME_SLOT"):
                time_slot_id = time_slot.get("TIME_SLOT_ID")
                time_value = time_slot.get("TIME_VALUE")
                time_slot_ids.append(time_slot_id)
                time_values.append(time_value)

        # Create a Pandas DataFrame
        dic_forTimeSlot = {'TIME_SLOT_ID': time_slot_ids, 'TIME_VALUE': time_values}
        timeSlot_df = pd.DataFrame(dic_forTimeSlot)

        lastTimeStamp = int(timeSlot_df.iloc[-1, 1])

        return lastTimeStamp
# final string




    def execute_phase_one(self):

        last_time_stamp: int = self.get_lastTime_stamp()
        current_participant_list: list = self.participant_list()
        myeafdf = self.pandas_whole
        laughter_Section = myeafdf[(myeafdf["Tier"] == "laughter_section")]
        '''
            [[147916, 149216, 'Discourse'],
             [174133, 175253, 'Discourse'],
             [180633, 181879, 'Mirthful'],
             [185740, 188462, 'Discourse '],
             [235600, 237111, 'Discourse'],
             [277201, 278945, 'Discourse '],
             [293055, 294279, 'Discourse'],
             [303763, 305672, 'Mirthful'],
             [323916, 325437, 'Discourse '],
             [353249, 355366, 'Discourse '],
             [372310, 374093, 'Discourse ']]

        '''
        laughter_list = self.pands_to_list_of_list(laughter_Section)
        Laughter_Annonatiion = '[laugh]'
        # for mergeing

        for item in current_participant_list:
            current_participant_id = item

            #   # Using katie algorithm



            current_participant_pd = myeafdf[(myeafdf["Tier"] == current_participant_id)]
            current_participant_link_list = Interlocutor_linklist()
            # -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
            #  Classification here
            for index, row in current_participant_pd.iterrows():

                star_time:int = row['Start Time']

                end_time:int = row['End Time']
                anonnation:str = row['Annotation'].strip()

                conversation_type = ''
                if star_time == 409795 and current_participant_id =='P007':
                    S =22
                # course
                if anonnation == Laughter_Annonatiion:
                    vocal_transfrom = Classfication_funtion(anonnation)

                    conversation_type:str = Laughter_Annonatiion
                    # Checking time here  and use interval tree here
                    #for checking time
                    search_interval = (star_time, end_time)
                    # Laughter
                    #new add
                    vocalisation_tag:str = self.vocalisation_classification(Laughter_Annonatiion)

                    # parameter to natural laughter verification
                    specific_laughter_cururent_speaker: str = self.change_abstruct_laughter_to_specific(search_interval,
                                                                                                        laughter_list)
                    #such as mirthful
                    # --------------------

                    #gemerate vocal sequence such as "V [V]."

                    vTag_sequence = vocal_transfrom.get_return()

                    speicifc_laughter = specific_laughter_cururent_speaker


                    #
                    #start megere
                    natural_laughter_verification_process = verification_to_natural_laughter(vTag_sequence,speicifc_laughter)

                    final_merge_type_laughter:str = natural_laughter_verification_process.get_return_str()

                    current_participant_link_list.add(star_time, end_time, final_merge_type_laughter)


                else:
                    # Vocalisation
                    # Annonation
                    #
                    laughter_marco: str = ' [laugh]'.strip()
                    #print(anonnation)
                    current_str = anonnation.strip()

                    vocal_transfrom = Classfication_funtion(current_str)

                    return_fromVocalTransfrom = vocal_transfrom.get_return()
                    # generate a [V] [laugh]
                    #


                    judge_cotain_laghter = laughter_marco in return_fromVocalTransfrom
                    # speaking laughter
                    if judge_cotain_laghter == True:
                        search_interval = (star_time, end_time)
                        # parameter to natural laughter verification
                        specific_laughter_cururent_speaker: str = self.change_abstruct_laughter_to_specific(search_interval,
                            laughter_list)

                        vTag_sequence = vocal_transfrom.get_return()

                        speicifc_laughter = specific_laughter_cururent_speaker
                        # merge type
                        natural_laughter_verification_process = verification_to_natural_laughter(vTag_sequence,
                                                                                                 speicifc_laughter)
                        final_merge_type_laughter_natural: str = natural_laughter_verification_process.get_return_str()

                        current_participant_link_list.add(star_time, end_time, final_merge_type_laughter_natural)
                    else:
                        current_participant_link_list.add(star_time, end_time, return_fromVocalTransfrom)


                        ######################################################################
                        ######################################################################
                        #  Adding SOCIAL LAUGHTER HERE  #

                    # toaddSocialLuaghter_button = self.judge_queryforSocialLaugher(filter_currentp_list, star_time,
                    #                                                                end_time)
                    # buttonToAdd = False
                    # if toaddSocialLuaghter_button == True:
                    #     social_laughter_Value = self.get_queryforSocialLaugher(filter_currentp_list,
                    #                                                            star_time, end_time)
                    #     myVocal_SOCIAL = verification_to_socicalLaughter(final_merge_type_laughter_natural, social_laughter_Value)
                    #     final_merge_type_laughter_social: str = myVocal_SOCIAL.process()

                    # ######################################################################
                    # ######################################################################
                    # if buttonToAdd == True:
                    #     current_participant_link_list.add(star_time, end_time, final_merge_type_laughter_social)

                    # else:

                    #     current_participant_link_list.add(star_time, end_time, final_merge_type_laughter_natural)

                     # Adding in the linklist
            # ----------------Finishe____-----------------------------------------------------------

            tempLastTimeNode = int(current_participant_link_list.identify_last_node_end_time())
            # last_time_stamp
            # system_last_time_stamp = self.get_lastTime_stamp()
            silience_type = 'Silience'
            if tempLastTimeNode < last_time_stamp:
                current_participant_link_list.add(tempLastTimeNode, last_time_stamp, silience_type)

            #  Finish loading translateinto into
            # | Partipaint iD | Start and end| time| duration |converaiont
            return_partipaint_list = current_participant_link_list.transform_linklist_to_listOflist()
            index_column = ['Start Time-ms', 'End time-ms', 'Duration-ms', 'Conversational Type']

            return_partipaint_df = pd.DataFrame(return_partipaint_list, columns=index_column)
            #  Fiinished inital construcion

            # Adding paricipant id and session id
            return_partipaint_df.insert(0, 'Participant ID', current_participant_id)
            # | Session id |Participant id | start |end |duaration | conversation type

            return_partipaint_df.insert(0, 'Session ID', self.session_id)

            return_partipaint_sorted_to_list = return_partipaint_df.values.tolist()

            if current_participant_id == current_participant_list[0]:
                self.moderator_list.append(return_partipaint_sorted_to_list)
            elif current_participant_id == current_participant_list[1]:
                self.participant_one_list.append(return_partipaint_sorted_to_list)

            elif current_participant_id == current_participant_list[2]:
                self.participiant_two_list.append(return_partipaint_sorted_to_list)


    def get_session_paricpat_df(self):
        modeerlIST = self.moderator_list
        transformed_list_moder_list = modeerlIST[0]
        # | Session id |Participant id | start |end |duaration | conversation type

        check_moderlist = transformed_list_moder_list[0]
        start_time_check = check_moderlist[2]
        if start_time_check != 0:
            new_start_time = 0
            new_end_time = start_time_check
            session_id = check_moderlist[0]
            participant_id = check_moderlist[1]
            new_duration = new_end_time - new_start_time
            conversation_type = 'Silience'
            temp_list = [session_id, participant_id, new_start_time, new_end_time, new_duration, conversation_type]
            transformed_list_moder_list.insert(0, temp_list)

        participat_one_list = self.participant_one_list

        transformed_list_p1_list = participat_one_list[0]
        check_p1 = transformed_list_p1_list[0]
        start_time_check = check_p1[2]
        if start_time_check != 0:
            new_start_time = 0
            new_end_time = start_time_check
            session_id = check_p1[0]
            participant_id = check_p1[1]
            new_duration = new_end_time - new_start_time
            conversation_type = 'Silience'
            temp_list = [session_id, participant_id, new_start_time, new_end_time, new_duration, conversation_type]
            transformed_list_p1_list.insert(0, temp_list)

        participat_two_list = self.participiant_two_list
        transformed_list_p2_list = participat_two_list[0]

        check_p2 = transformed_list_p2_list[0]
        start_time_check = check_p2[2]
        if start_time_check != 0:
            new_start_time = 0
            new_end_time = start_time_check
            session_id = check_p2[0]
            participant_id = check_p2[1]
            new_duration = new_end_time - new_start_time
            conversation_type = 'Silience'
            temp_list = [session_id, participant_id, new_start_time, new_end_time, new_duration, conversation_type]
            transformed_list_p2_list.insert(0, temp_list)

        my_merged_list_of_lists = transformed_list_moder_list + transformed_list_p1_list + transformed_list_p2_list
        self.all_paritcipant_list = my_merged_list_of_lists

        index_column = ['Session id', 'Participant id', 'Start Time-ms', 'End Time-ms', 'Duration-ms',
                        'CV-merge-M-L-S']

        current_df = pd.DataFrame(my_merged_list_of_lists, columns=index_column)
        filename = self.session_id+"_middle"+".csv"
        current_df.to_csv(filename)
        return current_df
        #  # | Session id |Participant id | start |end |duaration | conversation type
        #
        # index_column = ['Session id','Participant id','Start Time-ms','End Time-ms','Duration-ms','Conversation type']










