import re

# Usage for
class verification_to_socicalLaughter:
    #input seuqnece such as "[laugh]-Mirthful [V]."
    # special_laugher :Unratified Ratifying Ratified
    def __init__(self, input_str_vocoalSequence: str, specific_laughter_tag: str):
        self.input_str_vocoalSequence =input_str_vocoalSequence
        self.specific_laughter_tag = specific_laughter_tag
        self.return_str = self.process()
    def split_on_first_punctuation(self, s: str, punctuations: list) -> tuple:

        pattern = '[' + re.escape(''.join(punctuations)) + ']'

        match = re.search(pattern, s)
        if match:
            index = match.start()
            return s[:index], s[index:]
        else:
            return s, None

    def get_return_str(self) -> str:
        return self.return_str


    def process(self)->str:

        input_str:str= self.input_str_vocoalSequence
# [[laugh] V]
        word_List: list[str] = input_str.split()
        new_list: list[str] = []
        laughter_marco: str = ' [laugh]'.strip()
        punctuations_symbols: list[str] = ['!', '?', '.', ';', ',']

        # split puncation
        # processing wordlist
        for current_str in word_List:

            # judge
            puncationSymbolJudge = any(symbol in current_str for symbol in punctuations_symbols)
            if puncationSymbolJudge == True:
                # split the current string
                part1, part2 = self.split_on_first_punctuation(current_str, punctuations_symbols)
                new_list.append(part1)
                new_list.append(part2)
            else:
                # jduge laugher or normal V
                judge_contain_laugher_tag = laughter_marco in current_str
                if judge_contain_laugher_tag == True:
                    #split current string by -
                    hypen = '-'
                    list_spilit_current_str:list[str] = current_str.split('-')
                    part1_list = list_spilit_current_str[0]
                    part2_list = list_spilit_current_str[1]
                    mergeLaughter = part1_list+hypen+part2_list+hypen+self.specific_laughter_tag
                    new_list.append(mergeLaughter)

                else:
                    new_list.append(current_str)

        return_str = ''
        size_of_list_store = len(new_list)
        if size_of_list_store == 1:
            return_str = new_list[0]
        else:
            for index_list, curr_str in enumerate(new_list):

                if index_list != (size_of_list_store - 1):
                    # discuss the case
                    if index_list == 0:
                        return_str += curr_str

                    else:
                        # (0,lastOne)
                        # if thie one is not puncation add spaces
                        # if not do not add
                        if curr_str in punctuations_symbols:
                            return_str += curr_str
                        else:
                            return_str += ' '
                            return_str += curr_str

                else:
                    # Lastone
                    # judge symbol
                    if curr_str in punctuations_symbols:

                        return_str += curr_str
                    else:
                        return_str += ' '
                        return_str += curr_str

        return return_str

if __name__ == '__main__':
    vocal_str = '[laugh]-Mirthful V.'
    speicial_laghter='Ratiying'
    myVocal = verification_to_socicalLaughter(vocal_str,speicial_laghter)

    returnsss = myVocal.process()
    print("Before processsing"+vocal_str)
    print("After processing "+returnsss)